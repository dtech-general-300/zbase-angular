import { ILazyModule } from '../core/xconfig';


/** the lazy modules array, be careful with not repeated paths */
export const lazyModules: ILazyModule[] = [
  {
    path: 'user',
    loadChildren: () => import('../forms/user/user.module').then(m => m.UserModule)
  },
  {
    path: 'group',
    loadChildren: () => import('../forms/group/group.module').then(m => m.GroupModule)
  },

  // productos
  {
    path: 'inventario.product',
    loadChildren: () => import('../custom/product/product.module').then(m => m.ProductModule)
  },
  {
    path: 'inventario.productattributeline',
    loadChildren: () => import('../custom/product-attribute/product-attribute.module').then(m => m.ProductAttributeModule)
  },

  // patrones
  {
    path: 'inventario.patron',
    loadChildren: () => import('../custom/inventario/patron/patron.module').then(m => m.PatronModule)
  },
  {
    path: 'inventario.patronlinea',
    loadChildren: () => import('../custom/inventario/patronlinea/patronlinea.module').then(m => m.PatronlineaModule)
  },
  {
    path: 'inventario.patronatributo',
    loadChildren: () => import('../custom/inventario/patronatributo/patronatributo.module').then(m => m.PatronatributoModule)
  },
  {
    path: 'inventario.patronvalor',
    loadChildren: () => import('../custom/inventario/patronvalor/patronvalor.module').then(m => m.PatronvalorModule)
  }

];
