// use this interface when you want to pass data to
// a lazy loaded module root component and you dont
// use any kind of state management
export interface IForm {
  data: any;
}
