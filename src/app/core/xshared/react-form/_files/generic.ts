// this file contains the basic for a reactive forms

import { FormGroupDirective, FormBuilder, FormArray, FormGroup } from '@angular/forms';
import { BaseForm } from './base';
import { ViewChild, Injector, OnInit, Output, EventEmitter, AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { GenericQuery } from 'src/app/core/model/state/generic.query';
import { FocusMonitor } from '@angular/cdk/a11y';
import { take } from 'rxjs/operators';
import { from } from 'rxjs';
import { maingq, popupgq } from 'src/app/core/model/state/generic.token';




/** o2m and m2m relational fields need FormArray to work, FormArrays only shows the controls that they have */
export abstract class GenericForm extends BaseForm implements OnInit, AfterViewInit {

  @ViewChild('formRef', { static: false }) formRef: FormGroupDirective;
  @ViewChild('firstFocus', { static: false }) firstFocus: any;
  @ViewChildren('relational') relational: QueryList<any>; // relational fields
  @Output() outputEvent: EventEmitter<string> = new EventEmitter<string>();
  fields: any; // metadata of fields
  definition: { [key: string]: any }; // structure of all form

  protected gQuery: GenericQuery;
  protected fb: FormBuilder;
  protected focusMonitor: FocusMonitor;

  constructor(private injectorObj: Injector) {
    super();

    // Dependency Injection
    this.gQuery = this.injectorObj.get(maingq);
    this.fb = this.injectorObj.get(FormBuilder);
    this.focusMonitor = this.injectorObj.get(FocusMonitor);

    // Initialization
    this.fields = this.gQuery.getFormFields();
  }




  // BASIC METHODS

  ngOnInit() {
    // setup with definition
    this.setUpGroup();
    // emit when "extended class" ngOninit has finished (call super at end of ngOnInit "extended class")
    this.outputEvent.emit('OnInit');
  }

  ngAfterViewInit() {
    // emit event to indicate viewchilds have been executed
    this.outputEvent.emit('AfterViewInit');
  }

  focusFirst() { // if you change the logic of this method, also change similar method like o2mfocusFirst
    if (this.firstFocus) {
      if (this.firstFocus.focus) {
        this.focusMonitor.focusVia(this.firstFocus.focus, 'program');
      } else {
        // when widget has a ficticious form control or group, this use #pFocus internally
        this.firstFocus.internalFocus();
      }
    }
  }




  // FIELDS

  // set up group with definition object without fill "2many relational" fields, but filling "m2o fields"
  setUpGroup() {
    const realGroup = {};
    for (const fieldname in this.definition) {
      if (['one2many', 'many2many'].indexOf(this.fields[fieldname].relation) > -1) { // 2many relational fields
        realGroup[fieldname] = this.fb.array([]);
      } else if (this.fields[fieldname].relation === 'many2one') { // m2o fields
        realGroup[fieldname] = this.fb.control(
          [this.definition[fieldname][0]] // just put the default value for m2o, the validators are passed via html
        );
      } else { // normal fields
        realGroup[fieldname] = this.definition[fieldname];
      }
    }
    this.group = this.fb.group(realGroup); // set group
  }

  // prepare relational arrays spaces (if data == null just clear arrays)
  relationalSpaces(data: any = null) {
    for (const fieldname in this.definition) {
      if (['one2many', 'many2many'].indexOf(this.fields[fieldname].relation) > -1) { // 2many relational fields
        const rArray = this.fc[fieldname] as FormArray;
        rArray.clear(); // always clear relational fields
        if (data) {
          data[fieldname].forEach(() => { // just push n groups, based on length of array
            rArray.push(this.fb.group({
              id: [null], // add id in order know original value
              to_many: [null], // allow to check CRUD status of the record: null(readed), updated, created
              ...this.definition[fieldname]
            }));
          });
        }
      }
    }
  }

  // FIELDS METHODS

  /**
   * Wait for all o2m and m2m, until disableAll() have done for all that fields,
   * so when saveSubmit method in form.component is called, it will wait until
   * the correct data (managed in disableAll) is available.
   */
  async ready() {
    await Promise.all(this.relational.toArray().map(async (field) => { // resolve all in parallel
      await from(field.ready.pipe(take(1))).toPromise();
    }));
  }

}


/** o2m and m2m relational fields need FormArray to work, FormArrays only shows the controls that they have */
export abstract class GenericList {

  fields: any; // metadata of fields
  groupModel: any; // the base for FormArray items
  group: FormGroup; // a group that has only one element: a formarray
  listArray: FormArray; // reference for an array of groups
  definition: { [key: string]: any }; // structure of fields with validators

  protected gQuery: GenericQuery;
  protected fb: FormBuilder;

  constructor(private injectorObj: Injector) {
    // standard Dependency Injection
    this.fb = this.injectorObj.get(FormBuilder);
  }

  // Init
  // call explicity this method immediately after "createComponent", change queryToken if you want (ex: popup)
  init(queryToken = maingq) {
    // custom Dependency Injection
    this.gQuery = this.injectorObj.get(queryToken);

    // initialization
    this.fields = this.gQuery.getListFields();
    this.group = this.fb.group({ listArray: this.fb.array([]) });
    this.listArray = this.group.controls.listArray as FormArray;

    // setup
    this.setUpGroup();
  }

  // FIELDS

  // set up array with definition object without fill "2many relational" fields, but filling "m2o fields"
  setUpGroup() {
    const realGroup = {};
    for (const fieldname in this.definition) {
      if (['one2many', 'many2many'].indexOf(this.fields[fieldname].relation) > -1) { // 2many relational fields
        realGroup[fieldname] = this.fb.array([]);
      } else if (this.fields[fieldname].relation === 'many2one') { // m2o fields
        // TODO: just put the default value for m2o, the validators are passed via ?? TODO
        realGroup[fieldname] = [this.definition[fieldname][0]];
      } else { // normal fields
        realGroup[fieldname] = this.definition[fieldname];
      }
    }
    this.groupModel = realGroup;
  }


  // save data to formArray
  syncAll(data: any = null) {
    // clear
    this.listArray.clear();

    if (data) { // if data == null just clear arrays
      // iter data to save
      for (const record of data) {
        this.listArray.push(this.fb.group({ id: [null], ...this.groupModel })); // add "id + groupModel" at the last position of array
        const lastgroup = this.listArray.at(this.listArray.length - 1) as FormGroup; // get the last group reference
        this.syncSingle(lastgroup, record); // update spaces and save data
      }
    }

  }

  // save data to formGroup
  syncSingle(group: FormGroup, recordData: any = null) {
    // update spaces
    for (const fieldname in this.groupModel) { // just iter over all fieldname except id
      if (['one2many', 'many2many'].indexOf(this.fields[fieldname].relation) > -1) {
        const rArray = group.controls[fieldname] as FormArray;
        rArray.clear(); // always clear relational fields
        if (recordData && recordData[fieldname]) { // if recordData == null just clear arrays
          recordData[fieldname].forEach(() => { // just push n groups, based on length of array
            rArray.push(this.fb.group({
              id: [null], // add id in order know original value
              to_many: [null], // allow to check CRUD status of the record: null(readed), updated, created
              ...this.definition[fieldname]
            }));
          });
        }
      }
    }

    // save
    if (recordData) {
      group.patchValue(recordData);
    } else {
      group.reset();
    }
  }

  // // // // FIELDS METHODS

  // // // /**
  // // //  * Wait for all o2m and m2m, until disableAll() have done for all that fields,
  // // //  * so when saveSubmit method in form.component is called, it will wait until
  // // //  * the correct data (managed in disableAll) is available.
  // // //  */
  // // // // async ready() {
  // // // //   await Promise.all(this.relational.toArray().map(async (field) => { // resolve all in parallel
  // // // //     await from(field.ready.pipe(take(1))).toPromise();
  // // // //   }));
  // // // // }

}





