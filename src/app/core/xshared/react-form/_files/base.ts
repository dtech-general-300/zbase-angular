// this file contains the basic for a reactive forms

import { FormGroup } from '@angular/forms';

export abstract class BaseForm {
  group: FormGroup;

  // convenience getter for easy access to form fields
  get fv() { return this.group.value; }
  get fc() { return this.group.controls; }
}
