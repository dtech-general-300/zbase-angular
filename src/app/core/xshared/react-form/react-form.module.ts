import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';


@NgModule({
  declarations: [],
  exports: [
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class ReactFormModule { }
