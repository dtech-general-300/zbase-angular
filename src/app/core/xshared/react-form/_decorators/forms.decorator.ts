import { AbstractControl } from '@angular/forms';



/**
 * allow a function to return true when a field has an error
 * this result is used in a mat-error ngIf, in this case the function
 * has to be called with a zero in the first parameter. Other result
 * is given when the function has "1" as parameter, then return a string with
 * mapped error messages
 * @param errors Tuple array [error string, message string] when error string
 * is always, it means that its message string will always be shown when the
 * field has an error
 */
export function hasErrors(errors: [string, string][]): any {
  // tslint:disable-next-line: only-arrow-functions
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor): any {

    const originalMethod = descriptor.value; // el metodo original
    descriptor.value = function (...args: any[]) { // args written in function call
      const field: AbstractControl = originalMethod.apply(this, args); // return this.fc.control_name;
      if (args[0] === 0) {
        // return boolean
        return field.invalid && field.touched;
      } else {
        let result = '';
        errors.forEach(e => {
          if (field.hasError(e[0]) || e[0] === 'always') {
            result += (result === '' ? '' : ', ') + e[1];
          }
        });
        // return string
        return result;
      }
    };

  };
}

/**
 * the same as the other function but return the "error bool" and the "error message joined"
 * in a simple tuple that allows to avoid error on child widget on a reactive form
 */
export function fieldErrors(errors: [string, string][]): any {
  // tslint:disable-next-line: only-arrow-functions
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor): any {

    const originalMethod = descriptor.value; // el metodo original
    descriptor.value = function (...args: any[]) { // args written in function call
      const field: AbstractControl = originalMethod.apply(this, args); // return this.fc.control_name;
      const boolError = field.invalid && field.touched; // if the field has errors or not
      let textError = ''; // joined string with all error messages separated by commas
      errors.forEach(e => {
        if (field.hasError(e[0]) || e[0] === 'always') {
          textError += (textError === '' ? '' : ', ') + e[1];
        }
      });
      return [boolError, textError];
    };

  };
}


// SUBMIT

/**
 * Allows to avoid submitting if the form is invalid
 * and make all fields touched in order to make to appear
 * fields warnings
 */
export function canSubmit(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;
  descriptor.value = function (...args: any[]) {
    // submit stop here if form is invalid
    if (args[0].invalid) {
      args[0].markAllAsTouched();
      return;
    }
    return originalMethod.apply(this, args);
  };
}


/**
 * THE SAME AS canSubmit BUT THIS WORK WITH A COMPONENT REF
 * Allows to avoid submitting if the form is invalid
 * and make all fields touched in order to make to appear
 * fields warnings
 */
export function refSubmit(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;
  descriptor.value = function (...args: any[]) {
    if (args[0] === null) { // if the promise lazylaoder.load has not resolved yet, compRef initialized in null
      return;
    }
    // submit stop here if form is invalid
    if (args[0].instance.group.invalid) {
      args[0].instance.group.markAllAsTouched();
      return;
    }
    return originalMethod.apply(this, args);
  };
}
