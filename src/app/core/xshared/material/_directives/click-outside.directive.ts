import {
  Directive, Output, EventEmitter, ElementRef,
  OnDestroy, Input, OnInit, ChangeDetectorRef
} from '@angular/core';
import { Subject } from 'rxjs';
import { ClickOutsideService } from 'src/app/core/xservices';
import { takeUntil } from 'rxjs/operators';


// allows to detect when user click outside a component

@Directive({
  selector: '[appClickOutside]'
})
export class ClickOutsideDirective implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber
  @Input() checkDom = false; // wheter to check document.body or not (for dynamic UIs)
  @Output() appClickOutside = new EventEmitter<void>(); // the same directive as output

  constructor(
    private elementRef: ElementRef,
    private clickOutside: ClickOutsideService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.clickOutside.target$.pipe(takeUntil(this.u$)).subscribe(target => {
      this.onClick(target);
      this.cdr.markForCheck();
    });
  }


  // METHODS

  onClick(target: any) {
    // check body based on checkDom
    if (this.checkDom) {
      if (!this.existInBody(target)) { // if target not exist in page
        return;
      }
    }

    // check if element has target
    const clickedInside = this.elementRef.nativeElement.contains(target);
    if (!clickedInside) {
      this.appClickOutside.emit();
    }
  }

  // allow to check if a naviteElement exist inside html body
  existInBody(element) {
    // https://developer.mozilla.org/en-US/docs/Web/API/Node/contains
    return (element === document.body) ? false : document.body.contains(element);
  }


  // DESTROY

  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }

}
