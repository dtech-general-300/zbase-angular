import {
  trigger, animateChild, group,
  transition, animate, style, query, state
} from '@angular/animations';


// Routable animations
export const slideInAnimation =
  trigger('routeAnimation', [
    transition('desk <=> container', [
      style({ position: 'relative' }),
      query('@*', [animateChild()], { optional: true }),
      query(':enter, :leave', [
        animateChild(),
        style({
          position: 'absolute',
          top: 0,
          left: 0,
          width: '100%'
        })
      ]),
      query(':enter', [animateChild(),
      style({ left: '-100%' })
      ]),
      query(':leave', animateChild()),
      group([
        query(':leave', [animateChild(),
        animate('300ms ease-out', style({ left: '100%' }))
        ]),
        query(':enter', [animateChild(),
        animate('300ms ease-out', style({ left: '0%' }))
        ])
      ]),
      query(':enter', animateChild()),
    ])
  ]);



export const fadeOut = trigger('fadeOut', [
  state('void', style({
    opacity: 0
  })),
  transition('* => void', animate('210ms')),
]);
