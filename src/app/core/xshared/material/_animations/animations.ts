import {
  trigger, state, group,
  transition, animate, style
} from '@angular/animations';

// this class make possible to import in other files
// like components and easily manage animations

// TODO: maybe an IMPROVE is not use this class and just
// constants for each animations, it will prevent to
// import all class and just the needed code

export class Animations {
  static fadeInOut = trigger('fadeInOut', [
    state('void', style({
      opacity: 0
    })),
    transition('void => *', animate('500ms')),
  ]);

  static rotate90 = trigger('rotate90', [
    state('collapsed', style({ transform: 'rotate(0deg)' })),
    state('expanded', style({ transform: 'rotate(90deg)' })),
    transition('expanded <=> collapsed',
      animate('170ms cubic-bezier(0.4,0.0,0.2,1)')
    ),
  ]);

  static flyInOutX = trigger('flyInOutX', [
    state('in', style({
      width: 120,
      transform: 'translateX(0)', opacity: 1
    })),
    transition('void => *', [
      style({ width: 10, transform: 'translateX(50px)', opacity: 0 }),
      group([
        animate('0.3s 0.1s ease', style({
          transform: 'translateX(0)',
          width: 120
        })),
        animate('0.3s ease', style({
          opacity: 1
        }))
      ])
    ]),
    transition('* => void', [
      group([
        animate('0.3s ease', style({
          transform: 'translateX(50px)',
          width: 10
        })),
        animate('0.3s 0.2s ease', style({
          opacity: 0
        }))
      ])
    ])
  ]);

  static fadedGrowY = trigger('fadedGrowY', [
      state('void', style({ height: '0px', opacity: 0 })),
      transition('void => *', [
        group([
          animate('0.3s ease-out', style({height: '*'})),
          animate('0.3s ease-in', style({opacity: 1}))
        ])
      ]),
      transition('* => void', [
        group([
          animate('0.3s ease-in', style({height: '0px'})),
          animate('0.3s cubic-bezier(0.1, 0.75, 0.1, 0.75)', style({opacity: 0}))
        ])
      ])
  ]);

}
