/**
 * Convert an array of objects to an object of objects
 * @param key indica que key de cada object del array sera el key del nuevo object
 */
export const arrayToObject = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    };
  }, initialValue);
};
