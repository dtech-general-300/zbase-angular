// COMMON SHARED FUNCITONALITIES

export { arrayToObject } from './common/convertion';


// MATERIAL MODULE

export { Animations } from './material/_animations/animations';
export { slideInAnimation } from './material/_animations/route-animations';
export { fadeOut } from './material/_animations/route-animations';
export { MaterialModule } from './material/material.module';

// REACT MODULE

export { ReactFormModule } from './react-form/react-form.module';
export { BaseForm } from './react-form/_files/base';
export { GenericForm, GenericList } from './react-form/_files/generic';
export { hasErrors, canSubmit, refSubmit, fieldErrors } from './react-form/_decorators/forms.decorator';
