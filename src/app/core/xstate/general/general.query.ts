import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { GeneralStore, GeneralState } from './general.store';

@Injectable({ providedIn: 'root' })
export class GeneralQuery extends Query<GeneralState> {

  breadcrumbs$ = this.select('breadcrumbs');
  cargando$ = this.select('cargando');


  constructor(protected store: GeneralStore) {
    super(store);
  }

  // Breadcrumbs

  getCurrentCrumb() {
    const breadcrumbs = this.getValue().breadcrumbs;
    return (breadcrumbs.length > 0) ? breadcrumbs[breadcrumbs.length - 1] : null;
  }

  getprevCrumb() { // call only when u are sure that exist a prev crumb (like cancel in form)
    const breadcrumbs = this.getValue().breadcrumbs;
    return (breadcrumbs.length > 1) ? { crumb: { ...breadcrumbs[breadcrumbs.length - 2] }, index: breadcrumbs.length - 2 } : null;
  }

}
