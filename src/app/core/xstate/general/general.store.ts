import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

// INTERFACES

export interface BreadCrumb {
  name: string;
  type: string;
  route: string;
  params?: any;
  last?: boolean;
}


// STORE

export interface GeneralState {
  breadcrumbs: BreadCrumb[]; // keeps in order the links and internal params to previous in-modules
  cargando: number; // number of api requests loading
}

export function createInitialGeneralState(): GeneralState {
  return {
    cargando: 0,
    breadcrumbs: [],
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'general' })
export class GeneralStore extends Store<GeneralState> {

  constructor() {
    super(createInitialGeneralState());
  }

  // BREADCRUMBS

  resetBreadcrumbs() {
    this.update({ breadcrumbs: [] });
  }

  // CARGANDO

  addRequest(quantity: number) {
    this.update({
      cargando: this.getValue().cargando + quantity
    });
  }
}

