import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GeneralStore, BreadCrumb } from './general.store';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class GeneralService {

  constructor(
    private generalStore: GeneralStore
  ) { }

  // BREADCRUMBS

  addBreadcrumb(mode: string, bread: BreadCrumb) {
    let crumbs = this.generalStore.getValue().breadcrumbs; // the original

    switch (mode) {
      case 'override':
        crumbs = [...crumbs.slice(0, -1)]; break; // delete last
      case 'reset':
        crumbs = []; break; // empty
      case 'add': break; // dont do nothing
    }

    const breadcrumbs = [];
    crumbs.forEach(crumb => breadcrumbs.push({ ...crumb, last: false })); // all false
    breadcrumbs.push({ ...bread, last: true }); // just last = true

    this.generalStore.update({ breadcrumbs });
  }

  removeCrumbs(index: number) { // remueve todos los crumbs desde un index
    const breadcrumbs = this.generalStore.getValue().breadcrumbs.slice(0, index + 1);
    this.generalStore.update({ breadcrumbs });
  }


  // CARGANDO

  /** cargando box shows or hide with an animation */
  addCargando() {
    this.generalStore.addRequest(1);
  }

  substractCargando() {
    this.generalStore.addRequest(-1);
  }

}
