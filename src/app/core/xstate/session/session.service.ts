import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StandardInterception, baseURL } from '../../xconfig/http-config';
import { SessionStore, SessionState } from './session.store';
import { Router, ActivatedRoute } from '@angular/router';
import { shareReplay } from 'rxjs/operators';


// data structure returned from server
export interface ISession {
  message: string;
  user: SessionState;
}


@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private route: ActivatedRoute,
    private authStore: SessionStore
  ) { }

  login(email: string, pass: string) {
    this.authStore.setLoading(true);

    // request
    const body = {
      username: email,
      password: pass
    };
    const res$ = this.http.post<ISession>(
      baseURL + '/api_auth/login/',
      body,
      { headers: StandardInterception } // Standard Interception adds many headers
    ).pipe(shareReplay(1)); // many suscriptions will get data with only one http request

    // handle observable
    res$.subscribe(
      data => {
        // redirect
        const params = this.route.snapshot.queryParams;
        if (params.redirectURL) {
          this.router.navigate([params.redirectURL], { // note: this is like work with current routes in router
            queryParams: { redirectURL: null }, // overrides and clean (by merge) the redirectURL
            queryParamsHandling: 'merge', // allow to merge the route query params and this params
            preserveFragment: true, // allow to include original fragment
            replaceUrl: true // prevent that back button returns to login
          });
        } else {
          this.router.navigate(['/admin']); // when user type directly login in browser URL
        }
        // store session, because login was successful
        this.authStore.setSession(data.user);
      },
      () => {
        // error: don't do nothing
      }
    ).add(() => this.authStore.setLoading(false)); // when unsuscribe (all done) set loading to false
    return res$;
  }


  logout() {
    this.authStore.setLoading(true);

    // request
    const body = {};
    const res$ = this.http.post<ISession>(
      baseURL + '/api_auth/logout/',
      body,
      { headers: StandardInterception }
    ).pipe(shareReplay(1)); // many suscriptions will get data with only one http request

    // handle observable
    res$.subscribe(
      () => {
        // reset session, it makes isLoggedIn false because token set null.
        this.authStore.reset();
      },
      () => {
        // error: don't do nothing
      }
    ).add(this.authStore.setLoading(false));

    return res$;
  }

}
