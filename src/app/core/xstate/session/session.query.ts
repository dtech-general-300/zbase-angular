import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { SessionStore, SessionState } from './session.store';

@Injectable({
  providedIn: 'root'
})
export class SessionQuery extends Query<SessionState> {
  // isLoggedIn$ = this.select(state => toBoolean(state.token)); // reactive (observable) status of login
  // name$ = this.select(state => ({ userName: state.username, firstName: state.first_name, lastName: state.last_name }));

  constructor(protected store: SessionStore) {
    super(store);
  }

  isLoggedIn() {
    // the verification is between the client datetime and session expire-date
    // TODO: consider UTC time zone in real cases.

    let exp = this.getValue().expires; // getValue get the current state of store
    if (exp === null) { return false; }

    const now = new Date();
    exp = new Date(exp);

    return (exp.getTime() - now.getTime() > 0); // exp - now = positivo = isloggedin
  }

  conectionVerified() {
    // ¿the conection was stablished when app started?
    return this.getValue().verified === 'unknown error' ? false : true;
  }

}
