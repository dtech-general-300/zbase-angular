import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

// State is the structure of the Store
export interface SessionState {
  id: number;
  username: string;
  first_name: string;
  last_name: string;
  email: string;
  expires: Date;
  verified: string;
}

// The initial or neutral value (state) of the Store
export function nullSessionState(): SessionState {
  return {
    id: null,
    username: null,
    first_name: null,
    last_name: null,
    email: null,
    expires: null,
    verified: null,
  };
}

// the store is a service that 'store' many states
// resettable true, allows to call .reset() to get back to initial state
@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'session', resettable: true })
export class SessionStore extends Store<SessionState> {
  constructor() {
    // initial Session State
    super(nullSessionState());
  }

  setSession(session: SessionState) {
    this.update(session);
  }

}
