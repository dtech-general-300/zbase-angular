import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MenuStore } from './menu.store';
import { Observable } from 'rxjs';
import { StandardInterception, baseURL } from '../../xconfig';
import { shareReplay } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MenuService {

  constructor(
    private menuStore: MenuStore,
    private http: HttpClient
  ) { }


  // DESK & MODULE MENUS

  getDesk(): Observable<any> {
    // request
    const res$ = this.http.get(
      `${baseURL}/api_meta/metamenu?query=[('menu_type','in',['desk', 'module'])]&order=['-depth','sequence']`,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    res$.subscribe(
      data => {
        this.menuStore.updateLoaded({ desk: this.nestedMenu(data) });
      },
      () => { }
    );

    return res$;
  }

  getModule(): Observable<any> {
    // request
    const res$ = this.http.get(
      `${baseURL}/api_meta/metamenu?query=[('parent_path','startswith','${
      this.menuStore.getValue().activeModule.parent_path
      }/')]&order=['-depth','sequence']`, // the slash '/' after activeModule.parent_path, filter only childs
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    res$.subscribe(
      data => {
        const nestedData = this.nestedMenu(data);
        this.menuStore.updateLoaded({
          [this.menuStore.getValue().activeModule.client_route]: nestedData
        });
      },
      () => { }
    );

    return res$;
  }

  /** set active module with a MenuItem. when from='resolver' we search the MenuItem to save it */
  setActiveModule(param, from = 'normal') {
    let activeModule = param;
    if (from === 'resolver') {
      activeModule = this.recursiveChilds({ children: this.menuStore.getValue().loaded.desk }, param);
      if (activeModule === undefined) { // no se encontro en desk
        // TODO: raise error, or direct to error
      }
    }
    this.menuStore.update({ activeModule });
  }

  /** set active in-module with a MenuItem. when from='resolver' we search the MenuItem in the loaded module to save it */
  setActiveInModule(param, from = 'normal') {
    if (this.menuStore.getValue().canFlag) { // only active other in-module when canFlag is enable
      let activeInModule = param;
      if (from === 'resolver') {
        const children = this.menuStore.getValue().loaded[this.menuStore.getValue().activeModule.client_route];
        activeInModule = this.recursiveChilds({ children }, param, true);
        if (activeInModule === undefined) { // undefinied or null OJO: no se encontro en el modulo activo
          // TODO: raise error, or direct to error
        }
      }
      // update active action too, because one in-module are related with one action always
      this.menuStore.update({ activeInModule, activeAction: activeInModule.action_id });
      // update in-module Flag
      this.inFlag(); // must be set false when you use it somewhere (like first view in model module)

    } else { // if canFlag disabled, save the in-module to activate
      this.canAux(param);
    }
  }

  canAux(canAux: any) {
    this.menuStore.update({ canAux });
  }

  canFlag(canFlag: boolean) {
    this.menuStore.update({ canFlag });
  }

  inFlag(inFlag = true) {
    this.menuStore.update({ inFlag });
  }

  /** makes easy find a menu in nested menus, based on clientRoute */
  recursiveChilds(item, clientRoute: string, inmod = false) {
    if (item.children && item.children.length) { // expandable item
      for (const child of item.children) {
        const ok = child.client_route && (
          (!inmod && child.client_route === clientRoute) // when module
          || (inmod && `${child.menu_type}/${child.client_route}` === clientRoute) // when in-module
        );
        if (ok) {
          return child;
        } else {
          const res = this.recursiveChilds(child, clientRoute, inmod);
          if (res) { return res; }
        }
      }
    }
    return null;
  }

  /**
   * Only works for desk and modules.
   * Need menus filtered by module path and ordered by -depth and sequence
   */
  nestedMenu(menus) {
    let setChilds = {};
    let getChilds = {};
    let prevDepth = 0;
    for (const menu of menus) {
      if (prevDepth !== menu.depth) { // detecta cambios en depth
        prevDepth = menu.depth;
        getChilds = setChilds;
        setChilds = {};
      }
      if (getChilds.hasOwnProperty(menu.id)) { // asign childrens before save current menu
        menu.children = getChilds[menu.id];
      }
      setChilds[menu.parent_id] = [...setChilds[menu.parent_id] || [], ...[menu]]; // append menu
    }
    return setChilds[Object.keys(setChilds)[0]]; // get the value (array of menus) of the only property
  }



  // ACTIONS

  getAction(): Observable<any> {
    const id = this.menuStore.getValue().activeInModule.action_id;

    // request
    const res$ = this.http.get<any>(
      `${baseURL}/api_meta/metaaction/${id}/`,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    res$.subscribe(
      data => {
        // convert view_mode
        data.view_mode = this.modeFormat(data.view_mode);
        // cache data
        this.menuStore.updateLoadedAction({ [data.id]: data }); // update loaded action
      },
      () => { } // TODO: handle errors
    );

    return res$;
  }

  getActionByRef(ref: string): Observable<any> {
    // request
    const res$ = this.http.get<any>(
      `${baseURL}/api_meta/metaaction/?query=[('ref', 'exact', '${ref}')]`,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    res$.subscribe(
      rData => {
        const data = rData[0]; // because http return an array of records

        // convert view_mode
        data.view_mode = this.modeFormat(data.view_mode);

        // cache data
        this.menuStore.updateLoadedAction({ [data.id]: data }); // update loaded action
      },
      () => { } // TODO: handle errors
    );

    return res$;
  }

  modeFormat(viewMode) {
    const formatted = [];
    for (const mode of viewMode.split(';')) {
      const rex = /([a-zA-Z0-9-_.]+)\[([^\]]*)]/.exec(mode); // match 'dotmodel' and '[x,y,...]'
      formatted.push({ model: rex[1], order: rex[2].split(',') });
    }
    return formatted;
  }

}
