import { Injectable } from '@angular/core';
import { Store, StoreConfig } from '@datorama/akita';

export interface MenuItem {
  id: number;
  ref: string;
  name: string;
  menu_type: string;
  sequence: number;
  icon?: string;
  action_id?: number;
  client_route?: string; // TODO: consider if client_route combinated with menu_type (and maybe with :module) must be unique?
  initial_route?: string;
  parent_path?: string;

  children?: MenuItem[];
}

export interface MenuState {
  loaded: { [key: string]: MenuItem[] }; // menus cache // keys are 'desk' or client_route
  activeModule: MenuItem; // current module in the ui
  activeInModule: MenuItem; // current in-module in the ui

  loadedActions: { [key: number]: any }; // actions cache // keys are ids
  activeAction: number; //  id of current in-module action in the ui

  inFlag: boolean; // an in-module Flag that allow to know when imperatively an in-module was activated

  canFlag: boolean; // a flag that control whether an in-module can activated or not
  canAux: any; // the in-module that was supposed to activate if canFlag false
}

export function createInitialMenuState(): MenuState {
  return {
    loaded: {},
    activeModule: null,
    activeInModule: null,
    loadedActions: {},
    activeAction: null,
    inFlag: false,
    canFlag: true,
    canAux: null
  };
}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'menu' })
export class MenuStore extends Store<MenuState> {

  constructor() {
    super(createInitialMenuState());
  }

  /** allow to store loaded menus by desk and module */
  updateLoaded(loaded: Partial<MenuState['loaded']>) {
    this.update(state => ({
      loaded: {
        ...state.loaded,
        ...loaded
      }
    }));
  }

  /** allow to store loaded menus by desk and module */
  updateLoadedAction(loadedActions: Partial<MenuState['loadedActions']>) {
    this.update(state => ({
      loadedActions: {
        ...state.loadedActions,
        ...loadedActions
      }
    }));
  }

}

