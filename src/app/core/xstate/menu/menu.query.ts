import { Injectable } from '@angular/core';
import { Query } from '@datorama/akita';
import { MenuStore, MenuState } from './menu.store';

@Injectable({ providedIn: 'root' })
export class MenuQuery extends Query<MenuState> {

  InModuleMenuName$ = this.select(state => state.activeInModule.name);

  constructor(protected store: MenuStore) {
    super(store);
  }

  // MENUS

  /** allow to check if a menu was loaded, ref could be desk or a module route */
  isLoaded(ref: string) {
    return this.getValue().loaded.hasOwnProperty(ref);
  }

  /** return the specific loaded menu, ref could be desk or a module route */
  getLoadedMenu(ref: string) {
    return this.getValue().loaded[ref];
  }

  getActiveModule() {
    return this.getValue().activeModule;
  }

  getActiveInModule() {
    return this.getValue().activeInModule;
  }


  // ACTIONS

  isLoadedAction() {
    const id = this.getValue().activeInModule.action_id;
    return this.getValue().loadedActions.hasOwnProperty(id);
  }

  getActiveAction() {
    return this.getValue().loadedActions[this.getValue().activeAction];
  }

  getAction(id: number) {
    return this.getValue().loadedActions[id];
  }

  getActionByRef(ref: string) { // return action if found or false if not
    const actions = this.getValue().loadedActions;
    for (const action in actions) {
      if (actions[action].ref === ref) {
        return actions[action];
      }
    }
    return false;
  }

  /** when we have to load an in-module again (or first time). Indicates when an In-Module has been activated */
  inFlag() {
    return this.getValue().inFlag;
  }

  canFlag() {
    return this.getValue().canFlag;
  }

  canAux() {
    return this.getValue().canAux;
  }


  // SINGLE MODEL ACTION

  getModelName(actionId: number, index: number) {
    const action = this.getAction(actionId);
    return action.view_mode[index].model;
  }

  getView(actionId: number, index: number, viewType: string) {
    const action = this.getAction(actionId);
    return action.arch[action.view_mode[index].model][viewType];
  }


  getSingleModelName() {
    const action = this.getActiveAction();
    return action.view_mode[0].model;
  }

  getSingleView(viewType: string) { // when action have single model
    const action = this.getActiveAction();
    return action.arch[action.view_mode[0].model][viewType];
  }

}
