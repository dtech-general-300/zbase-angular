// WARNING: this module only has to be imported once in AppModule
// in order to add to root injector the httpInterceptorProviders
// and instantiate only once many packages like akita

import { NgModule, APP_INITIALIZER } from '@angular/core';

// http
import { HttpClientModule, HttpClientXsrfModule } from '@angular/common/http';
import { httpInterceptorProviders } from '../http-config';
const HTTP_MODS = [
  HttpClientModule, // this Module already include HttpClientXsrfModule
  // HttpClientXsrfModule.withOptions({ // change default-names
  //   cookieName: 'My-Xsrf-Cookie',
  //   headerName: 'My-Xsrf-Header',
  // }),
  HttpClientXsrfModule.disable(), // disable default XSRF Module, this will activate NoopInterceptor
  // https://github.com/angular/angular/blob/master/packages/common/http/src/interceptor.ts
  // https://github.com/angular/angular/blob/54e02449549448ebab6f255f2da0b4396665c6f0/packages/common/http/src/xsrf.ts#L76
];


// akita
// import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { environment } from 'src/environments/environment';


// initializers
import { SyncSessionDataService } from '../app-initializer-config/sync-session-data.service';
export function providerFactory(provider: SyncSessionDataService) {
  // https://devblog.dymel.pl/2017/10/17/angular-preload/
  // https://medium.com/@asfo/usando-app-initializer-en-angular-e822f3af3fb5
  return () => provider.load();
}


// manual lazy load
import { LAZY_MODS } from '../lazy-config/tokens';
import { lazyObject } from '../lazy-config/lazy-modules';


// model state management
import { mainToken } from '../../model/state/generic.token';




@NgModule({
  imports: [
    // http
    ...HTTP_MODS,
    // akita
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule.forRoot(),
  ],
  providers: [
    // http
    //
    // when this module is imported in root module 'AppModule'
    // the httpInterceptorProviders is added to root injector
    // making it accessible throughout the entire application
    httpInterceptorProviders,

    // akita
    //
    // {
    //   provide: NG_ENTITY_SERVICE_CONFIG,
    //   useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' }
    // },

    // initializers
    //
    // allow to verify session in order to avoid login again
    SyncSessionDataService,
    {
      provide: APP_INITIALIZER,
      useFactory: providerFactory,
      multi: true,
      deps: [SyncSessionDataService]
    },

    // Manual Lazy Load
    //
    { provide: LAZY_MODS, useFactory: lazyObject },


    // model tokens for state management (store,service,query)
    mainToken

  ],
})
export class InitConfigModule { }
