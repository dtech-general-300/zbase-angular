import {
  Injectable, Injector, Compiler, Inject,
  NgModuleFactory, Type, ViewContainerRef
} from '@angular/core';
import { LAZY_MODS } from './tokens';
import { timer } from 'rxjs';
import { componentFactoryName } from '@angular/compiler';


@Injectable({
  providedIn: 'root'
})
export class LazyLoaderService {

  constructor(
    @Inject(LAZY_MODS) private lazyModules: { [key: string]: () => Promise<NgModuleFactory<any> | Type<any>> },
    private injector: Injector,
    private compiler: Compiler,
  ) { }


  /**
   * manual "lazy load" and return component factory (this allow to "createComponent"
   * in the host component) (and allow to get component Factory in a resolver).
   * Use this method when you want to handle separately the process to get factory
   * and instantiate component.
   * @param entry is a string that indicates the static property for entrycomponent in the lazy module
   * @returns component Factory
   */
  async loadAndGetCompFactory(path: string, entry: string) {

    // get the loadChildren based on path name_ref // this is the manual lazy load
    const tempModule = await this.lazyModules[path]();

    // set moduleFactory based on AOT or JIT compiler
    let moduleFactory: NgModuleFactory<any>;
    if (tempModule instanceof NgModuleFactory) {
      // For AOT
      // console.log('aot');
      moduleFactory = tempModule;
    } else {
      // For JIT
      // console.log('jit');
      moduleFactory = await this.compiler.compileModuleAsync(tempModule);
    }

    // get entryComponent from module class based on a string that points to static method
    const entryComponent = (moduleFactory.moduleType as any)[entry];

    // using the angular injector generate the module using Factory, we inject all the dependencies in the module
    const moduleRef = moduleFactory.create(this.injector);
    // OJO: you can use here moduleRef.instance to do something

    // using the module, generate a component Factory. We are Resolving the entry component
    const compFactory = moduleRef.componentFactoryResolver.resolveComponentFactory<any>(entryComponent);

    return compFactory;
  }

  /**
   *  Load inside the container a manual lazy module
   *  component using the path as reference to get it.
   *  And pass data to the component (which need @Input).
   *  Use this method when you want to wrap all process
   *  to the get component.
   *  @returns component reference
   */
  async load(path: string, entry: string, container: ViewContainerRef, data: any = null) {

    // get component Factory
    const compFactory = await this.loadAndGetCompFactory(path, entry);

    // using the component factory, instantiate the component using the container
    const compRef = container.createComponent(compFactory);

    // use component instance to pass data to it (this is optional, only when data argument is set)
    // when data is set, the component must have an @Input() data:any;
    if (data !== null) {
      compRef.instance.data = data;
    }

    // run change detection explicity (because we are using OnPush) in order to allow the component
    // to be loaded inside the container (in the view). Because we are using a moduleRef with angular
    // injector we are using the  "componentFactoryResolver" from this moduleRef, not an explicit
    // componentFactResolver from the hostview (FormComponent of MODELmodule) which comes from the
    // MODELmodule; this provoke that we obtain a componentRef of the child component not the host
    // component. So when we call compRef.changeDetectorRef iis the change detector of the child comp.
    // If we use the componentRef from a compFactoryResolver injected in the host component, we will
    // obtain the change detector if host component if we call compRef.changeDetectorRef (in that case
    // the view has no effects) to solve that case we could do: (1) something like we did in this load
    // method, (2) update the view with markforcheck in a setter if we are passing data to component,
    // (3)we can get a ref of component injector "ref.injector.get(ChangeDetectorRef).markForCheck()"
    // in the host component, that allow us to run change detection that affect the child component.
    compRef.changeDetectorRef.markForCheck(); // only mark once before child component init // maybe not necesary

    // return compRef in order the parent access data of child comp.
    return compRef;
  }




  // PRELOADS

  /**
   * Just run the "loadChildren arrow function" with delay time and that provoke the bundle is lazy loaded
   * this preload Allow to run preload in "background", por decirlo asi.
   */
  preload(path: string, delay: number) {
    timer(delay).subscribe(
      x => {
        this.lazyModules[path]();
      }
    );
  }

  /**
   * Just run the "loadChildren arrow function" and that provoke the bundle is lazy loaded
   * This preload allow to manage a promise to wait until lazy module is loaded.
   */
  async asyncPreload(path: string) {
    await this.lazyModules[path]();
    return true; // i'll not use this value, 'cause I just wanna preload
  }

}
