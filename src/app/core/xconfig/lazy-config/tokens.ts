import { InjectionToken } from '@angular/core';

export const LAZY_MODS = new InjectionToken<{ [key: string]: string }>('LAZY_MODS');
