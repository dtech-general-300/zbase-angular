import { NgModuleFactory, Type } from '@angular/core';
import { lazyModules } from '../../../meta/lazy-modules';

// OUTCORE CALL


/** Interface for a lazy module */
export interface ILazyModule {
  path: string;
  loadChildren: () => Promise<NgModuleFactory<any> | Type<any>>;
}


/** the core lazy modules array, be careful with repeated paths */
export const lazyCoreModules: ILazyModule[] = [
  // {
  //   path: 'name_ref',
  //   loadChildren: () => import('../../').then(m => m.SomeModule)
  // }
];


/** convert IlazyModule arrays into an object */
export function lazyObject() {
  const lazyArray = [...lazyCoreModules, ...lazyModules ];
  const result = {};
  for (const w of lazyArray) {
    result[w.path] = w.loadChildren;
  }
  return result;
}


// TODO: make a mini-system that allows to include many arrays of ILazyModules
// in different parts of the app, and in the useFactory of the provider, join
// all of them making a unique provider that allows lazy-loader service called
// from everywhere in the app.
// An easier approach is: make a file in _metadata that joins all IlazyModule[]
// arrays, and in this file call that joined array, and use it in the funcion
// lazyObject that will be used in useFactory in the provider. (i have done this
// but maybe is a better approach, but i think it is ok)
