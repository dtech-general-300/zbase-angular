// Este servicio sirve para obtener datos de la session
// del usuario para usarlos en el tiempo de vida de la
// applicacion (de uno a otro refresh) y comprobar que
// la session este activa con el dato expire-date.

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StandardInterception, baseURL } from '../http-config';
import { ISession } from '../../xstate';
import { SessionStore } from '../../xstate/session/session.store';


/**
 * can't user SessionService because it uses Router and ActivatedRoute
 * which are not created yet in APP_INITIALIZER provider process
 * SessionStore use directly instead of SessionService
 */
@Injectable()
export class SyncSessionDataService {

  constructor(private http: HttpClient, private authStore: SessionStore) { }

  load() {
    return new Promise((resolve, reject) => {

      // request
      this.http.get<ISession>(
        baseURL + '/api_auth/verify/',
        { headers: StandardInterception }
      ).subscribe(
        data => {
          this.authStore.setSession(data.user); // store user session in akita
        },
        error => {
          if (error.status !== 403) {
            this.authStore.update({ verified: 'unknown error' }); // this will redirect to error page
          }
          // otherwise (error==403 forbidden): don't do nothing, this will cause guard redirect to login
        }
      ).add(() => resolve(true)); // resolve promise when observable has finished

    });
  }

}
