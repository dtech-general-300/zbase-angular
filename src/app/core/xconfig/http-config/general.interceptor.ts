import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import { GeneralService } from '../../xstate/general/general.service';
import { tap } from 'rxjs/operators';


@Injectable()
export class GeneralInterceptor implements HttpInterceptor {

  constructor(private generalService: GeneralService) { }

  // the selection_header is set in index.ts
  // this interceptor allows to add several selection_headers in index.ts file
  // and then set an appropiate Header or property for that selection_header

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
    // this debugger allows to see if HttpClientXsrfModule is disabled or not
    // if the flow goes through class 'NoopInterceptor', is disabled
    // debugger;



    const sel = req.headers.get('SelectionHeader');
    if (sel !== null) {

      this.generalService.addCargando(); // add 1 to cargando, -1 in final interceptor
      let cloneObject: any = {};

      if (sel.includes('content')) {
        cloneObject.setHeaders = { ...cloneObject.setHeaders, 'Content-type': 'application/json' };
      }

      if (sel.includes('credentials')) {
        cloneObject = { ...cloneObject, withCredentials: true };
      }

      if (Object.keys(cloneObject).length !== 0) {
        return next.handle(req.clone(cloneObject));
      }
    }

    return next.handle(req);
  }
}
