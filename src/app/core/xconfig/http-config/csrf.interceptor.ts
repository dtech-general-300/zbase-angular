import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpXsrfTokenExtractor
} from '@angular/common/http';


@Injectable()
export class CsrfInterceptor implements HttpInterceptor {

  constructor(private tokenExtractor: HttpXsrfTokenExtractor) {
  }

  // the selection_header is set in index.ts
  // this interceptor only allows to review the selection_header 'csrf'
  // in order to send to backend a Header with the token in unsafes methods
  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    const sel = req.headers.get('SelectionHeader');
    if (sel !== null) {

      if (sel.includes('csrf')) {
        // get token with default angular xsrf cookie name
        const token = this.tokenExtractor.getToken() as string;
        const unsafe = ['POST', 'PUT', 'PATCH', 'DELETE'];

        if (token !== null && unsafe.includes(req.method)) {
          return next.handle(req.clone({ setHeaders: { 'X-CSRFTOKEN': token } }));
        }
      }

    }

    return next.handle(req);
  }
}
