/// Barrel" of Http Interceptors
import { HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';

import { GeneralInterceptor } from './general.interceptor';
import { CsrfInterceptor } from './csrf.interceptor';
import { FinalInterceptor } from './final.interceptor';

// Http interceptor providers in outside-in order (first to last)
export const httpInterceptorProviders = [
  // This executes first
  { provide: HTTP_INTERCEPTORS, useClass: GeneralInterceptor, multi: true },
  // This executes second
  { provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true },
  // This executes third
  { provide: HTTP_INTERCEPTORS, useClass: FinalInterceptor, multi: true }, // always final, to remove selection_header
];

// Here you have to define the specific interceptors with selecion headers
export const StandardInterception = new HttpHeaders({ SelectionHeader: 'csrf.content.credentials' });
export const CsrfOnlyInterception = new HttpHeaders({ SelectionHeader: 'csrf' });
export const ContCredInterception = new HttpHeaders({ SelectionHeader: 'content.credentials' });

// server url
// export const baseURL = 'http://192.168.1.39:8000';
// export const baseURL = 'http://192.168.1.24:8000';
export const baseURL = 'http://localhost:8000';
