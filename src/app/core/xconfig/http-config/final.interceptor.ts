import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';
import { GeneralService } from '../../xstate/general/general.service';
import { tap } from 'rxjs/operators';


@Injectable()
export class FinalInterceptor implements HttpInterceptor {

  constructor(private generalService: GeneralService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    // the selection_header is set in index.ts
    // the final interceptor just remove the selection header
    // and handle parallel actions

    if (req.headers.has('SelectionHeader')) {
      const headers = req.headers.delete('SelectionHeader');

      return next.handle(req.clone({ headers })).pipe(tap(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) { // if a response ok on any request
            this.generalService.substractCargando(); // -1 cargando, it was added in general interceptor
          }
        },
        (err: any) => {
          // if an error ocurr on any request
          this.generalService.substractCargando(); // in this situation we must substract too
        }
      ));

    }

    return next.handle(req);

  }
}
