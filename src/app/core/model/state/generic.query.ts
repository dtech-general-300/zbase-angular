import { Injectable } from '@angular/core';
import { QueryEntity, EntityUIQuery } from '@datorama/akita';
import { GenericStore, GenericState, GenericUIState } from './generic.store';

@Injectable()
export class GenericQuery extends QueryEntity<GenericState> {
  ui: EntityUIQuery<GenericUIState>;

  // FORM REACTIVE
  formMode$ = this.select(state => state.gui.formMode);
  activeOrder$ = this.select(state => state.ids.indexOf(+this.getActiveId()) + 1);

  // LIST REACTIVE
  columns$ = this.select(state => state.gui.list); // all list fields metadata
  columnNames$ = this.select(state => [...state.gui.list.map(f => f.fieldname)]); // columns name string
  chips$ = this.select(state => state.gui.filterChips);
  pagination$ = this.select(state => {
    const page = state.gui.paginator;
    return [`${page.start} - ${page.end}`, `/ ${page.count}`, page.count];
  });


  constructor(protected store: GenericStore) {
    super(store);
    this.createUIQuery();
  }

  getModelName() {
    return this.getValue().gui.model;
  }

  // FORM IMPERATIVE

  getFormMode() {
    return this.getValue().gui.formMode;
  }

  getFormFields() {
    return this.getValue().gui.form;
  }

  /** a flag that permite that form ui just update once when necesary without redundancy */
  uiFlag() {
    return this.getValue().gui.uiFlag;
  }

  getTitle() {
    const entity: any = this.getEntity(+this.getValue().active);
    return entity ? entity.str_name : 'Nuevo';
  }


  // LIST

  getListFields() {
    return this.getValue().gui.listobj;
  }

  // LIST SELECTED

  getSelectCount() {
    return this.ui.selectCount(ent => ent.selected); // cuenta cuantos estan seleccionados
  }

  getSelectedIds() {
    return this.ui.getAll({
      filterBy: entity => entity.selected
    }).map((entity: any) => entity.id);
  }

  getSelected(id: number) {
    return this.ui.getEntity(id).selected; // retorna si un id esta seleccionado
  }

  selectSelected() {
    return this.ui.select(state => state.entities);
  }

  // LIST FILTERS

  getFilterDomain() {
    return this.getValue().gui.filterDomain;
  }

  getChips() {
    return this.getValue().gui.filterChips;
  }

  // LIST PAGINATOR

  getPaginator() {
    return this.getValue().gui.paginator;
  }

  getEmpty() {
    return this.getValue().gui.paginator.count === 0;
  }

}
