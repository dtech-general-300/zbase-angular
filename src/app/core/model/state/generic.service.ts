import { Injectable } from '@angular/core';
import { GenericStore, Generic, Chips } from './generic.store';
import { OrArray } from '@datorama/akita';
import { arrayToObject } from '../../xshared/common/convertion';
import { BreadCrumb } from '../../xstate';
import { Router, ActivatedRoute } from '@angular/router';



/**
 * this service is tied to generic-store and query which are not singletons,
 * instead are instances created in each component like list or from.
 */
@Injectable()
export class GenericService {

  constructor(
    private genericStore: GenericStore,
    private router: Router,
  ) { }


  goCrumb(crumb: BreadCrumb, index: number, route: ActivatedRoute) {
    let pg = null;
    switch (crumb.type) {
      case 'list': this.setChips(crumb.params.chips); pg = crumb.params.pagination; break;
      case 'form': break;
    }
    this.router.navigate([crumb.route], {
      relativeTo: route,
      state: {
        index,
        ...pg ? { limit: pg.limit, offset: pg.offset } : {}
      },
    });
  }

  // LIST

  /** set list of entities, array avoid that order of entities is lost */
  setList(entities: any[]) {
    this.genericStore.set(entities);
  }

  /** update individual record ui, we need update selected for list */
  updateSelection(ids: OrArray<number>, selected: boolean) {
    this.genericStore.ui.update(ids, { selected });
  }

  /** root UI: set the columns for list view */
  updatePaginator(pag: any) {
    const bottom = pag.current + pag.limit;
    const last = (Math.ceil(pag.count / pag.limit) - 1) * pag.limit; // last page (last offset)
    this.genericStore.updateRootUI({
      paginator: {
        previous: pag.previous !== null ? pag.previous : last, // prev offset
        current: pag.current, // current offset
        next: pag.next !== null ? pag.next : 0, // next offset
        limit: pag.limit, // page size
        count: pag.count, // total count of registers
        start: pag.current + 1, // start-end/count
        end: bottom > pag.count ? pag.count : bottom,
        enabled: !(pag.next === null && pag.previous === null) // if navigation trought pages is posible
      }
    });
  }

  setPaginator(paginator: any) {
    this.genericStore.updateRootUI({ paginator });
  }


  setFilterDomain(domain: string) { // allow to update filter without updating chips (be careful)
    this.genericStore.updateRootUI({ filterDomain: domain });
  }

  setChips(filterChips: { chips: Chips[], domain: string }) {
    this.genericStore.updateRootUI({ filterChips: filterChips.chips });
    this.setFilterDomain(filterChips.domain); // when chips change, filterdomain must change
  }

  // FORM

  /** create or update an entity */
  upsertRecord(id: number | null, entity: Generic) {
    this.genericStore.upsert(id, entity);
  }

  /** set an entity as active, by id */
  setActive(id: number | null) {
    this.genericStore.setActive(id);
  }

  /** create or update the active entity */
  upsertActive(entity: Generic) {
    this.genericStore.upsert(entity.id, entity);
  }

  /**
   * move to another entity
   * @param direction set 'prev' or 'next' entity as active
   * @param wrap  default=true, if false: when next in the last record, not go to first record
   */
  goActive(direction: 'prev' | 'next', wrap = true) {
    const wr = wrap ? {} : { wrap: false };
    const dr = direction === 'next' ? { next: true } : { prev: true };
    this.genericStore.setActive({ ...dr, ...wr });
  }

  setFormMode(mode: 'read' | 'edit' | 'create') {
    this.genericStore.updateRootUI({ formMode: mode });
  }

  uiFlag(uiFlag = true) {
    this.genericStore.updateRootUI({ uiFlag });
  }

  remove(ids: OrArray<number>) {
    this.genericStore.remove(ids);
  }


  // Entities

  // add(entities: OrArray<Generic>) {
  //   this.genericStore.add(entities);
  // }

  // update(ids: OrArray<number>, entity: Partial<Generic>) {
  //   this.genericStore.update(ids, entity);
  // }

  // remove(ids: OrArray<number>) {
  //   this.genericStore.remove(ids);
  // }


  // General State

  updateFormArch(arch: any, modelName) {
    this.genericStore.updateRootUI({
      model: modelName,
      form: arrayToObject(arch.fields, 'fieldname'), // convert to object
      header: arch.header
    });
  }

  updateListArch(arch: any, modelName) {
    this.genericStore.updateRootUI({
      model: modelName,
      list: arch.fields,
      listobj: arrayToObject(arch.fields, 'fieldname'), // convert to object
    });
  }


  resetFormMode() {
    this.genericStore.updateRootUI({
      formMode: null,
      uiFlag: false
    });
  }

  reset() {
    this.genericStore.resetAll();
  }

  destroy() {
    this.genericStore.destroy();
  }

}
