/**
 * Put here all the tokens that will use certain instances of generic services(service, store, query).
 * useClass create new instance everytime it is provided, but if you want configuration for each instance
 * you must use useFactory:
 * https://dev.to/reibo_bo/create-a-reusable-angular-service-c0m,
 * https://www.youtube.com/watch?v=tA9tuKbg5Yk
 * https://stackblitz.com/edit/angular-multi-provider-example
 * If you want to use an existing instance of a provided service, just use @Inject(token) in DI of the class.
 */
import { GenericService } from './generic.service';
import { GenericStore } from './generic.store';
import { GenericQuery } from './generic.query';
import { InjectionToken } from '@angular/core';
import { Router } from '@angular/router';


// factories
export function gsFactory(store: GenericStore, router: Router): GenericService {
  return new GenericService(store, router);
}

export function gqFactory(store: GenericStore): GenericQuery {
  return new GenericQuery(store);
}


/////////////////////
// MAIN
/////////////////////

// tokens
export const maingt = new InjectionToken<GenericStore>('Token.GenericStore');
export const maings = new InjectionToken<GenericService>('Token.GenericService');
export const maingq = new InjectionToken<GenericQuery>('Token.GenericQuery');

// array
// for easily provide in InitConfigModule (app root) (it is the same as provideIn:'root' but the difference is that
// now we can get new instances of the same akita store,service,query for other popups or something. We must provide
// this mainToken in root because resolvers always are provided in root, if we not provide in root error will rise
// cause resolvers need this service,query to work.)
export const mainToken = [
  // useClass create a new instance
  { provide: maingt, useClass: GenericStore },
  // deps usa los tokens para inyectar en la nueva instancia de GenericSerivce
  { provide: maings, useFactory: gsFactory, deps: [maingt, Router] },
  // deps usa los tokens para inyectar en la nueva instancia de GenericQuery
  { provide: maingq, useFactory: gqFactory, deps: [maingt] },
];



/////////////////////
// POPUP
/////////////////////

// tokens
export const popupgt = new InjectionToken<GenericStore>('PopupToken.GenericStore');
export const popupgs = new InjectionToken<GenericService>('PopupToken.GenericService');
export const popupgq = new InjectionToken<GenericQuery>('PopupToken.GenericQuery');

// array
export const popupToken = [
  // useClass create a new instance
  { provide: popupgt, useClass: GenericStore },
  // deps usa los tokens para inyectar en la nueva instancia de GenericSerivce
  { provide: popupgs, useFactory: gsFactory, deps: [popupgt, Router] },
  // deps usa los tokens para inyectar en la nueva instancia de GenericQuery
  { provide: popupgq, useFactory: gqFactory, deps: [popupgt] },
];
