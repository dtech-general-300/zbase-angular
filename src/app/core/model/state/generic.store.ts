import { Injectable, OnDestroy } from '@angular/core';
import { EntityState, EntityStore, guid, EntityUIStore, ActiveState } from '@datorama/akita';

// STORE ENTITY ROOT

/** entity model: only id because it is generic */
export interface Generic {
  id: number;
}

export interface Chips { // list filter-group-favorite chips
  label: string;
  fieldname: string;
  text: string;
  domain: string;
}

/** the store state extended with root or global vars for UI */
export interface GenericState extends EntityState<Generic, number>, ActiveState {
  gui: {
    // general
    model: string; // model name, ejemp:'api.user'
    // form
    form: any; // form names in object type (allow to access data by name of name)
    header: any; // header of the form (buttons and state bar)
    formMode: string; // the state of edition in form (read, create, edit)
    uiFlag: boolean // allow to update UI inside combineLatest of form view
    // list
    list: any[]; // orderer list columns
    listobj: any; // the same as list but in object, like form
    paginator: any; // all paginator parameters
    filterDomain: string; // string with filter domain (filter means: filter-group-favorite,etc)
    filterChips: Chips[]; // array of filter chips (filter means: filter-group-favorite,etc)

  };
}
/** initial state for root ui */
const initialRootState = {
  gui: {
    model: 'new',
    form: {},
    header: [],
    formMode: null,
    uiFlag: false,
    list: [],
    listobj: {},
    paginator: {},
    filterDomain: '',
    filterChips: [],
  }
};


// INTERNAL UI STORE FOR EACH ENTITY

/** entity-base UI state, which belong to each record */
export interface GenericUI {
  selected: boolean; // list selected
}
/** Entity ui state */
export interface GenericUIState extends EntityState<GenericUI, number> { }
/** when the normal entities are set, all selected=false */
const initialUIState = {
  selected: false
};


@Injectable()
export class GenericStore extends EntityStore<GenericState> {
  ui: EntityUIStore<GenericUIState>;

  constructor() {
    super(initialRootState, { name: `G-${guid()}`, resettable: true }); // set initial state and a variable name
    this.createUIStore({}, { resettable: true }).setInitialEntityState(initialUIState);
  }

  updateRootUI(gui: Partial<GenericState['gui']>) { // allow to pass partial object to update root UI
    this.update(state => ({
      gui: {
        ...state.gui,
        ...gui
      }
    }));
  }

  resetAll() {
    this.reset();
    this.ui.reset();
  }

}
