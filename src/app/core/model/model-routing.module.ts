import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { ContainerComponent } from './container/container.component';

import { FormDeactivateGuard } from './resolver/form-deactivate.guard';
import { InFormResolver } from './resolver/form-resolver.service';
import { InListResolver } from './resolver/list-resolver.service';
import { ListDeactivateGuard } from './resolver/list-deactivate.guard';



const routes: Routes = [
  {
    path: '',
    component: ContainerComponent,
    children: [
      {
        path: '',
        redirectTo: '/admin',
        pathMatch: 'full' // TODO: make all /admin/:module/type redirect to admin
      },
      {
        path: ':model',
        component: ListComponent,
        canDeactivate: [ListDeactivateGuard],
        resolve: { list: InListResolver },
        runGuardsAndResolvers: 'always'
        // runGuardsAndResolvers: allow to runGandR, when navigate to the same route
        // runGuardsAndResolvers: (child routes will run resolvers too, be careful)
      },
      {
        path: ':model/:id',
        component: FormComponent,
        canDeactivate: [FormDeactivateGuard],
        resolve: { form: InFormResolver },
        runGuardsAndResolvers: 'always'
        // runGuardsAndResolvers: allow to runGandR, when navigate to the same route
        // runGuardsAndResolvers: (child routes will run resolvers too, be careful)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModelRoutingModule { }
