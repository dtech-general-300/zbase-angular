import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { LazyLoaderService } from '../../xconfig';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MenuQuery } from '../../xstate';


/**
 * En este modulo se instancian los servicios GENERIC que sirven
 * para que en estos se almacenen los datos de cada modelo que se
 * vaya usando (en cada list view y form view).
 */
@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  constructor(
    // @Inject(maings) private gService: GenericService,
    private lazyLoader: LazyLoaderService,
    private menuQuery: MenuQuery,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {

    // react to route changes, it work at :model level: '/admin/:module/model/:model'
    // this does not run when navigate to the same route (like a refresh) when click a menu item
    // only runs when :model is changed
    this.route.firstChild.paramMap.pipe(takeUntil(this.u$)).subscribe((params: ParamMap) => {
      // preload form bundle (chunk) after the view is presented in viewport (when a list is loaded).
      // Though this run when :model changed, and maybe called many timer for the same :model, the
      // preload will not affect perfomance because the chunk is already loaded (angular cached chunks)
      this.lazyLoader.preload(this.menuQuery.getSingleView('form').arguments.form_path, 300);
    });


  }


  ngOnDestroy() {
    this.u$.next();
    this.u$.complete();
  }

}
