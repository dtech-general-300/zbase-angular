import {
  Component, OnInit, OnDestroy, ViewChild,
  ViewContainerRef, ComponentRef, ChangeDetectionStrategy, Inject
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Observable, of, combineLatest } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { refSubmit } from '../../xshared';
import { userConfirm } from '../../xguards';
import { ModelHttp } from '../../xservices';
import { MenuQuery, BreadCrumb, GeneralQuery } from '../../xstate';
import { GenericService } from '../state/generic.service';
import { GenericQuery } from '../state/generic.query';
import { maingq, maings } from '../state/generic.token';


// This works with onpush
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  @ViewChild('host', { read: ViewContainerRef, static: true }) host: ViewContainerRef; // dynamic components
  compRef: ComponentRef<any> = null;
  mode: string;

  recordName: string;
  rActive$: Observable<number>; // numero de registro activo (segun el orden del list view)
  rTotal$: Observable<number>; // cantidad de registros en el list view

  // bread
  breadcrumbs$: Observable<BreadCrumb[]>;

  constructor(
    private modelHttp: ModelHttp,
    private menuQuery: MenuQuery,
    private router: Router,
    private route: ActivatedRoute,
    @Inject(maingq) private gQuery: GenericQuery,
    @Inject(maings) private gService: GenericService,
    private generalQuery: GeneralQuery,
  ) { }

  ngOnInit() {
    // observables
    this.rActive$ = this.gQuery.activeOrder$;
    this.rTotal$ = this.gQuery.selectCount();
    this.breadcrumbs$ = this.generalQuery.breadcrumbs$;

    // combined obs (need each obs emit at least one value), it reacts to formMode change (when suscribe emit 1 value)
    // and reacts to resolvers (when suscribe receive data from resolvers). CombineLatest avoid code redundancy.
    // uiFlag can be used in 2 ways 1)when formMode change without navigate 2)in resolver after navigate uiFlag is set
    combineLatest([this.gQuery.formMode$, this.route.data]).pipe(takeUntil(this.u$)).subscribe(([mode, data]) => {
      this.mode = mode;

      // run when: the model is the same but navigated to "other record or new"; OR when formMode change
      if (this.compRef) {
        if (this.gQuery.uiFlag()) { // avoid repeated computation
          this.updateUiMode();
          this.focusFirst();
          this.gService.uiFlag(false);
        }
      }

      // run only once when: first access to form OR model changed
      if (!this.compRef) {
        this.host.clear(); // clear all content of host
        this.compRef = this.host.createComponent(data.form.compFactory); // instantiate child component
        this.compRef.instance.outputEvent.pipe(take(2)).subscribe(hook => { // suscribe to child events
          switch (hook) {
            case 'OnInit': this.updateUiMode(); break;
            case 'AfterViewInit': this.focusFirst(); break; // focus when create or edit
          }
        });
      }
    });
  }


  // UI

  updateUiMode() {
    const ins = this.compRef.instance;
    const active = this.gQuery.getActive();
    switch (this.mode) {
      case 'read':
        ins.relationalSpaces(active); // manage spaces in relational fields
        ins.group.reset(active, { emitEvent: false }); // just need to reset pristine & untouched to true
        ins.group.disable({ emitEvent: false }); break; // disable all makes valid = false
      case 'edit':
        ins.relationalSpaces(active); // manage spaces in relational fields
        ins.group.reset(active, { emitEvent: false }); // update ui values, and reset pristine & untouched to true
        ins.group.enable({ emitEvent: false }); break;
      case 'create':
        ins.relationalSpaces(); // manage spaces in relational fields
        (ins.formRef) ? ins.formRef.resetForm() : ins.group.reset({ emitEvent: false }); // resetForm == reset but also reset submitted
        ins.group.enable({ emitEvent: false }); break;
    }
  }

  focusFirst() { // only focus first in create or edit mode
    if (this.mode !== 'read') { this.compRef.instance.focusFirst(); }
  }



  // NAVIGATE

  breadCrumbs(crumb: any, index: number) {
    this.gService.goCrumb(crumb, index, this.route.parent.parent.parent); // parent.parent.parent because of paths of router
  }

  /** ask user if he want to discard changes */
  customDeactivate() {
    let canDeactivate = true;
    if (this.compRef.instance.group.dirty) { // si los fields fueron modificados
      canDeactivate = userConfirm('Desea descartar los cambios?');
    }
    return canDeactivate;
  }

  /** go to another record only in the scope of visible list */
  goRecord(go: 'prev' | 'next') {
    if (this.gQuery.getCount() > 1) { // avoid navigate to the same record
      if (this.customDeactivate()) { // in form-deactivate.guard: goActive run evenly if user cancel. This avoid that.
        this.compRef.instance.group.markAsPristine(); // in order to jump form-deactivate.guard (and ask again)
        this.gService.goActive(go);
        this.router.navigate(['../', this.gQuery.getActiveId()], { relativeTo: this.route });
      }
    }
  }



  // CRUD

  /**
   * Utiliza la ref del componente hijo para forzar el submit del hijo que puede hacer lo que sea,
   * aqui en cambio se ejecuta todo lo estándar, como guardar la informacion por un request http
   */
  @refSubmit
  async saveSubmit(x) {
    await this.compRef.instance.ready(); // wait for all relational fields to be ready

    let res$: Observable<any>;
    const modelRoute = this.menuQuery.getSingleView('form').arguments.api_route;
    const id = +this.gQuery.getActiveId();
    const group = this.compRef.instance.group;

    if (this.mode === 'create') {
      res$ = this.modelHttp.save(modelRoute, group.getRawValue()); // raw values of form (included disabled)
    } else { // edit (when guardar clicked in edit mode) // only patch if modified
      res$ = group.dirty ? this.modelHttp.modify(modelRoute, id, group.getRawValue()) : of(this.gQuery.getActive());
    }

    res$.subscribe(record => {
      group.markAsPristine(); // before navigate make child's group pristine, to avoid guard
      this.router.navigate(['../', record.id], { relativeTo: this.route, replaceUrl: true, state: { record } }); // avoid request again
    }).add(_ => {
      // submit child form // TODO: error handling, "add" runs even in error? or just when cmoplete and unsuscribe?
      // onSubmit must be trigger only when the request post or patch have been done correctly without error.
      this.compRef.instance.formRef.onSubmit(null);
    });
  }

  edit() {
    this.gService.uiFlag();
    this.gService.setFormMode('edit');
  }

  create() {
    this.router.navigate(['../new'], { relativeTo: this.route }); // resolver will make id=null
  }

  cancel() {
    if (this.mode === 'create') {
      const prev = this.generalQuery.getprevCrumb();
      this.gService.goCrumb(prev.crumb, prev.index, this.route.parent.parent.parent);
    } else { // edit (when cancel clicked in edit mode)
      if (this.customDeactivate()) { this.gService.uiFlag(); this.gService.setFormMode('read'); }
    }
  }

  delete(active: number) {
    if (userConfirm('Está seguro que desea eliminar este registro?')) {
      const modelRoute = this.menuQuery.getSingleView('form').arguments.api_route;
      const deleteId = +this.gQuery.getActiveId();

      this.modelHttp.delete(modelRoute, deleteId).subscribe(() => {
        const len = this.gQuery.getCount();
        if (len === 1) { // if no records (after delete), go to list
          const prev = this.generalQuery.getprevCrumb();
          if (prev.crumb.type === 'list') { prev.crumb.params = { ...prev.crumb.params, pagination: null }; } // to avoid errors
          this.gService.goCrumb(prev.crumb, prev.index, this.route.parent.parent.parent);
        } else {
          this.gService.goActive(len === active ? 'prev' : 'next', false); // stay in same position or in the border
          this.gService.remove(deleteId); // delete from store
          this.router.navigate(['../', this.gQuery.getActiveId()], { relativeTo: this.route, replaceUrl: true });
          // TODO: create a deactivate guard, or the same existing guard, with a skip back in history action in order
          // to avoid error when user press back button and router in browser go to the deleted id and then redirect to admin
          // or error. In this.router.navigate, replaceUrl just replace one eleement in history with deleted id, but maybe in
          // history there are more urls with the deleted id that provoke error when user press back button.
        }
      });

    }
  }

  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }

}
