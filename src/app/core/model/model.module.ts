import { NgModule } from '@angular/core';

import { ModelRoutingModule } from './model-routing.module';
import { ReactFormModule } from '../xshared';
import { ListComponent } from './list/list.component';
import { FormComponent } from './form/form.component';
import { ContainerComponent } from './container/container.component';
import { WidgetsModule } from './widgets';


@NgModule({
  declarations: [
    ListComponent,
    FormComponent,
    ContainerComponent
  ],
  imports: [
    ReactFormModule,
    ModelRoutingModule,
    WidgetsModule
  ]
})
export class ModelModule { }
