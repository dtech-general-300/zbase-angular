import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { userConfirm } from '../../xguards/common';
import { FormComponent } from '../form/form.component';

@Injectable({
  providedIn: 'root'
})
export class FormDeactivateGuard implements CanDeactivate<FormComponent> {
  canDeactivate(
    component: FormComponent, route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot, next: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let can = true;
    if (component.compRef.instance.group.dirty) { // si los fields fueron modificados
      can = userConfirm('Desea descartar los cambios?');
    }
    return can;
  }
}
