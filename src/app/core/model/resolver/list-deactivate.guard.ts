import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate } from '@angular/router';
import { ListComponent } from '../list/list.component';
import { MenuQuery, MenuService } from '../../xstate';


@Injectable({
  providedIn: 'root'
})
export class ListDeactivateGuard implements CanDeactivate<ListComponent> {

  constructor(
    private menuQuery: MenuQuery,
    private menuService: MenuService,
  ) { }

  async canDeactivate(
    component: ListComponent, route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot, next: RouterStateSnapshot
  ): Promise<boolean> {

    let can = true;
    if (!this.menuQuery.canFlag()) { // siempre que se esté editando (crear or update) el record
      if (component.listtable && component.listtable.lastRowClicked) { // si existe el record
        const record = component.listtable.data.controls[component.listtable.lastRowClicked];
        can = await component.cancel({ record }); // realiza todo el proceso de cancelacion si el usuario descarta
        if (can) { // si se descartó, se navega al sitio requerido al resolver este Deactivate pero tambien emulando el click del menu
          if (this.menuQuery.canAux()) { // esto emula el clic del menu cuando sea necesario
            this.menuService.setActiveInModule(this.menuQuery.canAux());
            this.menuService.canAux(null); // reset can auxiliar
          }
        }
      }
    }

    return new Promise(res => res(can));
  }
}
