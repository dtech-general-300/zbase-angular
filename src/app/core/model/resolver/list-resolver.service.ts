import { Injectable, Inject } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { from } from 'rxjs';
import { MenuQuery, GeneralService, MenuService } from '../../xstate';
import { resolverPipe, InModuleMenuResolver } from '../../xguards';
import { ModelHttp } from '../../xservices/model.http';
import { GenericService } from '../state/generic.service';
import { GenericQuery } from '../state/generic.query';
import { LazyLoaderService } from '../../xconfig';
import { maings, maingq } from '../state/generic.token';


/**
 * resolve in-module, then get data from server
 * TODO: when get action, retrieve with initial data
 */
@Injectable({
  providedIn: 'root'
})
export class InListResolver implements Resolve<any> {

  constructor(
    private router: Router,
    private menuQuery: MenuQuery,
    private menuService: MenuService,
    private mHttp: ModelHttp,
    private lazyLoader: LazyLoaderService,
    private inResolver: InModuleMenuResolver,
    @Inject(maingq) private gQuery: GenericQuery,
    @Inject(maings) private gService: GenericService,
    private generalService: GeneralService,
  ) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {

    // RESOLVE: in-module menus&action (cached)
    await from(this.inResolver.resolve(route, state)).toPromise();



    // RESOLVER: list data // TODO: Handle errors of responses
    const defaultChips = { chips: [], domain: '' }; // the default chips to be applied when inFlag==true
    if (this.menuQuery.inFlag()) { // when we have to reset view
      // reset FilterDomain with default domain (chips), (after resolver we will reset chips)
      this.gService.setFilterDomain(defaultChips.domain);
    }
    const extra = this.router.getCurrentNavigation().extras.state; // if extras, send custom pagination limit and offset
    const query = this.gQuery.getFilterDomain();
    const view = this.menuQuery.getSingleView('list');
    const mRoute = view.arguments.api_route;

    // RESOLVE in parallel
    const listPath = view.arguments.list_path;
    const [list, compFactory] = await Promise.all([
      // form data request
      from(resolverPipe(
        extra && extra.hasOwnProperty('limit') ?
          this.mHttp.fetchList(mRoute, query, '', extra.limit, extra.offset) :
          this.mHttp.fetchList(mRoute, query),
        state.url,
        this.router
      )).toPromise(),
      // faster if chunk already loaded in previous calls of listview
      this.lazyLoader.loadAndGetCompFactory(listPath, 'list')
    ]);




    // AFTER RESOLVE DATA

    if (this.menuQuery.inFlag()) { // when we have to reset view
      // clear flag
      this.menuService.inFlag(false);
      // generic
      this.gService.reset(); // clear all even chips
      this.gService.setChips(defaultChips); // set default chips and filterDomain (filter-groups-etc)
      // default breadcrumbs
      this.generalService.addBreadcrumb('reset', {
        name: this.menuQuery.getActiveInModule().name,
        type: 'list',
        route: this.menuQuery.getActiveInModule().menu_type + '/' + this.menuQuery.getActiveInModule().client_route,
        params: {
          pagination: null,
          chips: defaultChips,
          model: this.menuQuery.getSingleModelName()
        },
      });
    } else {
      // reset formMode to keep a good UX
      this.gService.resetFormMode();

      // remove if necesary
      if (extra && extra.hasOwnProperty('index')) { this.generalService.removeCrumbs(extra.index); }

      // update list breadcrumb
      this.generalService.addBreadcrumb('override', {
        name: this.menuQuery.getActiveInModule().name,
        type: 'list',
        route: this.menuQuery.getActiveInModule().menu_type + '/' + this.menuQuery.getActiveInModule().client_route,
        params: {
          pagination: extra ? { limit: extra.limit, offset: extra.offset } : null,
          chips: { chips: this.gQuery.getChips(), domain: this.gQuery.getFilterDomain() },
          model: this.menuQuery.getSingleModelName()
        },
      });
    }

    this.gService.updateListArch(view, this.menuQuery.getSingleModelName()); // here we update ModelName
    this.gService.setList(list.results);
    this.gService.updatePaginator(list);

    // FINISH RESOLVER METHOD
    return { list, compFactory };
  }

}

