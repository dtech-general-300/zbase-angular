import { Injectable, Inject } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, from, of } from 'rxjs';
import { resolverPipe } from '../../xguards/common';
import { LazyLoaderService } from '../../xconfig';
import { MenuQuery, GeneralService, GeneralQuery, MenuService } from '../../xstate';
import { ModelHttp } from '../../xservices/model.http';
import { InModuleMenuResolver } from '../../xguards';
import { GenericService } from '../state/generic.service';
import { GenericQuery } from '../state/generic.query';
import { maingq, maings } from '../state/generic.token';


// FETCH RESOLVER

/** The same principle of List resolver, it fetch data but only for 1 record */
@Injectable({
  providedIn: 'root'
})
export class FormDataResolver implements Resolve<any> {

  constructor(
    private modelHttp: ModelHttp,
    private menuQuery: MenuQuery,
    private router: Router,
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {

    // NEW
    if (route.paramMap.get('id') === 'new') {
      return of({ id: null }); // id=null significa nuevo registro, y define mode del form en 'create'
    }

    // AFTER CREATE OR EDIT
    const extra = this.router.getCurrentNavigation().extras.state;
    if (extra && extra.record) {
      return of(extra.record); // avoid another request with the same data when "save" (guardar)
    }

    // REQUEST DATA FROM SERVER
    const id = route.paramMap.get('id');
    const modelRoute = this.menuQuery.getSingleView('form').arguments.api_route;
    return resolverPipe(
      this.modelHttp.fetchRecord(modelRoute, +id), // replayed Observable
      state.url,
      this.router
    );

  }

}


/**
 * resolve in-module, then get data from server and load CompFactory
 * TODO: when get action, retrieve with initial data (for form)
 * TODO: handle errors when promises reject, if not handled en earch resolve function
 * TODO: just get the necesary fields (fields data that user can get access, with groups security Idont know, do it)
 */
@Injectable({
  providedIn: 'root'
})
export class InFormResolver implements Resolve<any> {

  constructor(
    private inResolver: InModuleMenuResolver,
    private formResolver: FormDataResolver,
    private menuQuery: MenuQuery,
    private menuService: MenuService,
    private lazyLoader: LazyLoaderService,
    @Inject(maingq) private gQuery: GenericQuery,
    @Inject(maings) private gService: GenericService,
    private generalService: GeneralService,
    private generalQuery: GeneralQuery,
    private router: Router,
  ) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {


    // RESOLVE: in-module menus&action (cached)
    await from(this.inResolver.resolve(route, state)).toPromise();


    // RESOLVE in parallel
    const formPath = this.menuQuery.getSingleView('form').arguments.form_path;
    const [result, compFactory] = await Promise.all([
      // form data request
      from(this.formResolver.resolve(route, state)).toPromise(),
      // faster if chunk already loaded in list view
      this.lazyLoader.loadAndGetCompFactory(formPath, 'form')
    ]);


    // AFTER RESOLVE DATA
    // update the record data (received from resolvers) 'cause we want show last data in server.
    // TODO: if a record was erased by other person, and we move to that record we have to show cached record, and
    // show an alert with only accpet button to return the usser to the list view (which is requested again).

    // update fields from action
    this.gService.updateFormArch(this.menuQuery.getSingleView('form'), this.menuQuery.getSingleModelName()); // here we update modelName

    // update formMode based on past formMode
    const prevMode = this.gQuery.getFormMode();
    if (true) { // TODO: model has changed (in if)
      switch (prevMode) { // dependiendo de donde se viene se setea el mode
        case 'edit': // if setFormMode not run, formMode will be the same 'edit'
          const extra = this.router.getCurrentNavigation().extras.state;
          if (extra && extra.record) { this.gService.setFormMode('read'); } // if comes from "save": change to read
          if (result.id === null) { this.gService.setFormMode('create'); }  // if new: change mode to create
          break;
        default: // when comes from create, read or null
          this.gService.setFormMode(result.id === null ? 'create' : 'read');
      }
    }


    // in case an in-module has been activated (user enter form directly from browser)
    if (this.menuQuery.inFlag()) {
      this.menuService.inFlag(false);

      // add list default breadcrumb
      this.generalService.addBreadcrumb('reset', {
        name: this.menuQuery.getActiveInModule().name,
        type: 'list',
        route: this.menuQuery.getActiveInModule().menu_type + '/' + this.menuQuery.getActiveInModule().client_route,
        params: { pagination: null, chips: { chips: [], domain: '' }, model: this.menuQuery.getSingleModelName() },
      });
    }

    // update Store
    this.gService.setActive(result.id); // no problem if 'set active' before the store have data.
    if (this.gQuery.getCount() === 0) { // in case the user enter form directly from browser
      if (result.id !== null) { this.gService.setList([result]); }// only setList if not new record
    } else {
      this.gService.upsertActive(result); // upsert active record with recently requested data (from server)
    }

    // update ui Flag
    // prevMode is null only when form component is instanced (in that situation, ui is updated based on child hooks)
    // when change model, compRef is undefined again, so it will update by hooks; we must not call uiFlag when model change
    // to avoid flag=true, which would trigger an unnecesary computation
    if (prevMode !== null) { this.gService.uiFlag(); }


    // update form breadcrumb
    // here, at least 1 breadcrumb exist. Only add when model or view changed (come from list), otherwise override
    const crumb = this.generalQuery.getCurrentCrumb(); // maybe never be null (at least 1 crumb)
    const modelChanged = crumb.params.model !== this.menuQuery.getSingleModelName();
    const viewChanged = crumb.type !== 'form';
    const crumbMode = modelChanged || viewChanged ? 'add' : 'override';

    const id = route.paramMap.get('id');
    const am = this.menuQuery.getActiveInModule();
    const fRoute = `${am.menu_type}/${am.client_route}/${id}`;

    this.generalService.addBreadcrumb(crumbMode, {
      name: this.gQuery.getTitle(),
      type: 'form',
      route: fRoute,
      params: { model: this.menuQuery.getSingleModelName() }
    });

    // FINISH RESOLVER
    return { result, compFactory };
  }

}
