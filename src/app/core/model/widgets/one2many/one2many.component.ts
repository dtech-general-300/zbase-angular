import {
  Component, OnInit, Input, ChangeDetectionStrategy, OnDestroy,
  ViewChildren, QueryList, Output, EventEmitter, ChangeDetectorRef, ViewChild, Inject
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { GenericQuery } from '../../state/generic.query';
import { takeUntil } from 'rxjs/operators';
import { FocusMonitor } from '@angular/cdk/a11y';
import { ActivatedRoute } from '@angular/router';
import { ListtableComponent } from '../base/list/listtable/listtable.component';
import { maingq } from '../../state/generic.token';


@Component({
  selector: 'app-one2many',
  templateUrl: './one2many.component.html',
  styleUrls: ['./one2many.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class One2manyComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  @Input() parent: FormGroup; // parent formGroup reference
  @Input() meta: any; // field metadata received from server
  @Input() def: { [key: string]: any }; // definition of field
  @Output() ready: EventEmitter<boolean> = new EventEmitter<boolean>();

  mode: string; // form Mode: edit, create, read

  // for listtable
  @ViewChild('listtable', { static: false }) listtable: ListtableComponent; // to get listtable variables
  config: any; // general config
  group: FormGroup;
  columnNames: string[]; // array of fieldnames in order to compose mat-table


  constructor(
    @Inject(maingq) private gQuery: GenericQuery,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    // list
    this.columnNames = this.meta.fields.map(field => field.fieldname);
    this.route.data.pipe(takeUntil(this.u$)).subscribe(() => { // after each resolver
      this.syncGroup();
    });

    // mode change
    this.gQuery.formMode$.pipe(takeUntil(this.u$)).subscribe(mode => {
      this.mode = mode;
      this.syncGroup();
    });

    // for listtable
    this.config = {
      def: this.def,
      selection: 'no',
      deletion: true,
      clickOut: true,
      escFunc: async (o) => await this.cancel(o),
      saveFunc: async (o) => await this.save(o),
      externalCreate: false,
      customName: this.meta.fieldname,
      style: {
        headerRowSticky: true,
        elevation: 'mat-elevation-z0',
        addButtonName: 'Agregar línea'
      }
    };
  }

  syncGroup() {
    this.group = { ...this.parent } as FormGroup;
    this.cdr.markForCheck(); // allow to update because of OnPUsh
  }

  // TABLE & Update

  // TODO: validation
  async save(out: any) {
    if (out.clickOut) { // solo si fue emitido con clickOut true
      return await new Promise(resolve => {
        this.listtable.disableAny(); // deshabilitar el row actual
        this.ready.emit(true); // to show that we already have disable any editing record
        resolve(true);
      });
    }
  }

  async cancel(out: any = null) {
    if (this.listtable.creating) { out.record.markAsPristine({ onlySelf: true }); }
    this.listtable.disableAny(); // disable the record and mark parent as dirty if necesary
    out.record.patchValue(this.listtable.prevData); // return the data of this record to the previous data before modify
    return true; // finish await of the caller of this function
  }


  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }
}
