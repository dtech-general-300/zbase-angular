import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { One2manyComponent } from './one2many.component';

describe('One2manyComponent', () => {
  let component: One2manyComponent;
  let fixture: ComponentFixture<One2manyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ One2manyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(One2manyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
