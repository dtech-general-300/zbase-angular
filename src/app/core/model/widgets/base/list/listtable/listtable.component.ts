import {
  Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef,
  QueryList, ViewChildren, ChangeDetectionStrategy, OnChanges, SimpleChanges, OnDestroy
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { FocusMonitor } from '@angular/cdk/a11y';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-listtable',
  templateUrl: './listtable.component.html',
  styleUrls: ['./listtable.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListtableComponent implements OnInit, OnChanges, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  @Input() parent: FormGroup; // the group that contains the formArray
  @Input() metafields: any[]; // metadata for each field (column)
  @Input() cNames: string[]; // columnNames array of column names for table
  @Output() cellclick: EventEmitter<any> = new EventEmitter<any>();

  @Input() check: any; // config for checkboxes
  @Output() master: EventEmitter<boolean> = new EventEmitter<boolean>(); // from master check toggle
  @Output() single: EventEmitter<any> = new EventEmitter<any>(); // from single check boxes

  @Input() config: any; // config object for different thing like styles or simple vars

  // editable
  @Input() editable: boolean; // the editable status
  @ViewChildren('dCell') dCells: QueryList<any>;
  @Output() bclick: EventEmitter<string> = new EventEmitter<string>(); // indicate which button clickeds
  @Output() emode: EventEmitter<string> = new EventEmitter<string>(); // indicate whether a record start editing or stop editing
  @Output() out: EventEmitter<any> = new EventEmitter<any>(); // to make posible click other buttons or things
  lastRowClicked: number; // the last index clicked
  prevData: any; // data before the record was edited
  creating = false; // flag to know if user is creating a record

  // general variables
  list: any[]; // array of objects, to use it on mat-table
  data: FormArray; // o2m field reference, it is an array of formGroups (from this, we extract data for mat-table)
  columnNames: string[];
  checkBool: any; // object that contains ui for selected like {1:{id:1, selected:false}, 2:...}


  constructor(
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef,
    private focusMonitor: FocusMonitor,
  ) { }

  ngOnInit() {
    if (this.check) { // cause not always check is wanted
      this.check.checks.pipe(takeUntil(this.u$)).subscribe(entities => {
        this.checkBool = entities;
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const cc = (i: string) => changes[i] && changes[i].previousValue !== changes[i].currentValue;
    const cd = (i1, i2) => changes[i1] && changes[i1][i2].previousValue !== changes[i1][i2].currentValue;

    // cName change, or editable change (for final_delete ), or config.selection 'multi' (for initial_select)
    if (cc('cNames') || cc('editable') || cd('config', 'selection')) {
      this.syncColumnNames();
    }

    // parent change
    if (cc('parent')) {
      if (this.config.customName) { // cause not always the name of array is listArray
        this.data = this.parent.controls[this.config.customName] as FormArray;
      } else {
        this.data = this.parent.controls.listArray as FormArray;
      }
      this.data.disable(); // disable each time ActiveRecord data change
      this.lastRowClicked = null; // to be sure that is null when data change
      this.syncList(); // synchronize list when parent change
      // }
    }
  }



  // NORMAL

  syncList() { // sync list with control values
    const newList = this.data.controls.map(_ => _.value);
    this.list = Array(4).fill({ xEmpty: true });
    this.list.splice(0, newList.length, ...newList); // override empty rows
  }

  checkType(variable: any) {
    return typeof (variable);
  }

  syncColumnNames() {
    this.columnNames = [
      ...this.config.selection === 'multi' ? ['initial_select'] : [],
      ...this.cNames,
      ...this.config.deletion && this.editable ? ['final_delete'] : []
    ];
  }



  // EDITABLE

  clickOutSide() { // tied with appClickOutside. (you must call disableAny from parent method save)
    // the index and record that was editting
    const index = this.lastRowClicked;
    const record = this.data.controls[this.lastRowClicked];

    // just emit current index + control, also clickOut to know if it clicked in "OTHER" area.
    // out event must be captured in the tag, and run the same function of saveFunc: (out)="save($event)"
    this.out.emit({ index, record, clickOut: this.config.clickOut });
  }

  disableAny() {  // disable lastRowClicked and make it null (you could emit a "ready signal" to show that disable has been done)
    if (this.editable) {
      this.disableRow(); // disable a row and patch it with created or updated
      this.lastRowClicked = null; // reset lastrow clicked
      this.emode.emit('stop'); // show that record return to disable state
    }
  }

  disableRow() { // disable a record
    const toMany = this.creating ? 'created' : 'updated';
    this.checkCreating(); // just for delete pristine newRecord (if exist)
    if (this.lastRowClicked !== null) { // if the previous record was in editmode
      const record = this.data.controls[this.lastRowClicked];
      record.disable();
      if (record.dirty) { // allways call after disable()
        if (record.value.to_many !== 'created') { record.patchValue({ to_many: toMany }); }
        this.data.markAsDirty(); // if you call disableRow before disable, this markAsDirty dont work
      }
    }
  }

  checkCreating() { // check: if user is creating, and if record is not dirty (pristine) we delete it
    if (this.creating) {
      this.creating = false; // we make false inmediately cause if not and 'delete' pressed makes an circular calling
      const index = this.data.length - 1;
      if (this.data.controls[index].pristine) { // if control has not been modified
        this.delete(index);
        this.lastRowClicked = null; // set null 'cause lastRowClicked was the one which has been creating
      }
    }
  }

  delete(rIndex: number) {
    if (this.config.bclick) { // emit instead of normal delete Method
      this.bclick.emit('delete');
    } else {
      this.disableAny(); // click delete button is the same effect as clickoutside (to save other active or remove pristine new)
      this.data.removeAt(rIndex);
      this.syncList();
      this.data.markAsDirty(); // the o2m field has modified, that means 'dirty'
    }
  }

  add() {
    if (this.config.bclick) { // emit instead of normal add Method
      this.bclick.emit('add');
    } else {
      // check if user was creating
      this.checkCreating();

      // add a new Record
      const newRecord = { to_many: ['created'], ...this.config.def };
      this.data.push(this.fb.group(newRecord));
      this.syncList();

      // focus on new record
      let firstNoReadonly = this.metafields.findIndex(field => !field.readonly); // the first no readonly record
      firstNoReadonly = firstNoReadonly < 0 ? 0 : firstNoReadonly; // if -1 (not found) then put 0
      this.cellCliked(this.data.length - 1, firstNoReadonly, newRecord, null); // go to firstNoReadonly subField & save previous record
      this.creating = true; // now user is creating a record
    }
  }

  async keys(type: string) { // keys pressed
    switch (type) {
      case 'shift+enter':
        const done = await this.xFunc(this.config.saveFunc);
        if (done) { this.add(); }
        break;
      case 'enter': await this.xFunc(this.config.saveFunc); break;
      case 'esc': await this.xFunc(this.config.escFunc); break;
    }
  }

  async xFunc(func) {
    if (func) {
      return await func({ index: this.lastRowClicked, record: this.data.controls[this.lastRowClicked], clickOut: this.config.clickOut });
    } else {
      this.disableAny();
      return true;
    }
  }


  // EVENTS

  singleCheck(value: boolean, id: number) {
    this.single.emit({ id, value });
  }

  async cellCliked(rIndex: number, cIndex: number, rowRecord: any, field: any) {
    if (rowRecord.xEmpty) { // empty row clicked
      // when click empty row it is the same as click outside (which run saveFunc)
      await this.xFunc(this.config.saveFunc);
    } else { // valid row clicked

      if (this.editable && !this.config.noClickable) { // when editable, edit on cell click
        if (rIndex !== this.lastRowClicked) {
          // for other functionalities
          this.emode.emit('start'); // the record now will be enabled
          this.prevData = this.data.controls[rIndex].value; // save prev data for use in the way that parent wants

          // set new lastRowClicked
          let permitido = true;
          if (!await this.xFunc(this.config.saveFunc) && this.lastRowClicked !== null) { // first save prev
            permitido = await this.xFunc(this.config.escFunc); // if fail to save, delete (this avoid garbage)
          }

          if (permitido) { // solo edita el registro seleccionado si el registro anterior ha sido guardado, o descartado
            this.data.controls[rIndex].enable(); // enable clicked
            this.lastRowClicked = rIndex; // setf clicked
            // detect change for new line
            this.cdr.detectChanges(); // allow to detect added line before focus dCell
            // focus
            const cell = this.dCells.toArray()[cIndex * this.data.length + rIndex];
            this.listFocus(cell);
          }
        }
      } else { // when no editable
        this.cellclick.emit({ rowRecord, field }); // emit an object with important data
      }

    }
  }

  // focus
  // this method has the same logic of generic.ts focusFirst (this is a system paradigm, there are normal fields and fields
  // which need to have a ficticious formcontrol in the ui to make operations before save data in original controls)
  listFocus(cell) {
    if (cell) {
      if (cell.focus) {
        this.focusMonitor.focusVia(cell.focus, 'program');
      } else {
        // when widget has a ficticious form control or group, this use #pFocus internally
        cell.internalFocus();
      }
    }
  }



  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }

}
