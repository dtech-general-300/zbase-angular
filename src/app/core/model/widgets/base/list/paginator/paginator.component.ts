import { Component, Input, ViewChild, ElementRef, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { FocusMonitor } from '@angular/cdk/a11y';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {

  u$ = new Subject<void>(); // unsuscriber

  @Input() show: boolean; // wether the paginator show or hide
  @Input() pagination$: Observable<string[]>;
  @Input() data$: Observable<any>; // es un observable que cuando emite se debe resetear edit (como señal de que consiguio paginar)

  @Output() dirclick: EventEmitter<string> = new EventEmitter<string>();
  @Output() ready: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('paginatorInput', { static: false }) paginatorInput: ElementRef<HTMLInputElement>;

  paginator = new FormControl('', paginationValidator(/^[0-9]+\s?-\s?[0-9]+$/));
  edit = 0; // wether to edit pagination or not (1,2 = true, 0=false)


  constructor(
    private focusMonitor: FocusMonitor,
  ) { }

  ngOnInit() {
    this.data$.pipe(takeUntil(this.u$)).subscribe(() => { // observable that indicate that data was received
      this.edit = 0;
    });
  }

  editPage(init = '') {
    if (this.edit === 1) { // when user click span appClickOutside also run
      this.edit = 2; // set mode 2 in order to avoid enter this if again
      this.paginator.enable(); // **just an effect of waiting
      this.paginator.markAsTouched(); // allow see red box when value is invalid
      this.paginator.setValue(init); // set init value (come from observable) cause clickoutside also run
      this.focusMonitor.focusVia(this.paginatorInput, 'program');
    } else {
      if (this.paginator.valid) { // only when user enter a valid value (regex pattern)
        this.paginator.disable(); // **just an effect of waiting
        const values = getStartEnd(this.paginator);
        this.ready.emit(values);
      } else {
        this.edit = 0;
      }
    }
  }

}


function getStartEnd(control: AbstractControl) {
  const [start, end] = control.value.replace(/\s/g, '').split('-').map(n => +n);
  return { start, end, valid: start <= end };
}

function paginationValidator(rex: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let valid = rex.test(control.value); // see if regex is ok
    valid = valid ? getStartEnd(control).valid : valid; // if start if higher than end
    // tslint:disable-next-line: object-literal-key-quotes
    return valid ? null : { 'validRange': { value: control.value } };
  };
}
