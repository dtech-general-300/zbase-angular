import {
  Component, ViewChild, ElementRef, OnDestroy, Output,
  EventEmitter, AfterViewInit, Input, OnChanges, SimpleChanges
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { userConfirm } from 'src/app/core/xguards';
import { MatAutocomplete } from '@angular/material';
import { Subject, ReplaySubject, of, Observable } from 'rxjs';
import { takeUntil, take, timeout, catchError } from 'rxjs/operators';
import { FocusMonitor } from '@angular/cdk/a11y';
import { Chips } from 'src/app/core/model';


/** run with OnPush */
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnChanges, OnDestroy, AfterViewInit {

  u$ = new Subject<void>(); // unsuscriber

  // chips
  filterFields: string[];
  searchChips: Chips[] = [];
  searchControl = new FormControl();
  text$ = this.searchControl.valueChanges;

  // allow to wait Keydown
  replaySub = new ReplaySubject<string>(1);
  replay$ = this.replaySub.asObservable();

  @ViewChild('filterInput', { static: false }) filterInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: false }) matAutocomplete: MatAutocomplete;

  // output to filter component
  @Output() domainEvent = new EventEmitter<{ chips: Chips[], domain: string }>(); // indicate that searchChips has been modified

  @Input() data$: Observable<any>; // observable that emit when data has been filtered and received (route.data)
  @Input() chips$: Observable<Chips[]>; // observable of chips (stored in akita or somewhere/)
  @Input() meta: any; //  metadata of the server view returned with an action



  constructor(
    private focusMonitor: FocusMonitor,
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    const cc = (i: string) => changes[i] && changes[i].previousValue !== changes[i].currentValue;

    if (cc('meta')) {
      // get search names
      this.filterFields = this.meta.fields.filter((field: any) => this.meta.search.fields.indexOf(field.fieldname) > -1);
    }

  }

  ngAfterViewInit() {

    // run after resolvers run or a observable that show that data has arrived
    this.data$.pipe(takeUntil(this.u$)).subscribe(() => {
      this.focus();
    });

    // chips from store
    this.chips$.pipe(takeUntil(this.u$)).subscribe(chips => {
      this.searchChips = [...chips]; // update chips
      this.searchControl.setValue(null); // reset control (text$ get null value)
      this.filterInput.nativeElement.value = ''; // clean input
    });
  }

  focus() {
    this.focusMonitor.focusVia(this.filterInput, 'program');
  }

  // FILTERS

  /** only runs (and wait) if autocomplete was opened and key pressed (shift+enter or enter) */
  async addSelected(input) {

    // wait for key events (timeout wait for replay$ to emit, osea keypressed)
    const keyref = await this.replay$.pipe(take(1), timeout(21), catchError(_ => of('enter'))).toPromise();

    // reset subject to make next waiting possible
    this.replaySub = new ReplaySubject<string>(1);
    this.replay$ = this.replaySub.asObservable();


    // SAVE

    // retrived data from option
    const metafield = this.searchControl.value.field; // metadata for field
    const newChip: Chips = { // newChip just have field and value
      label: metafield.label,
      fieldname: metafield.fieldname,
      text: this.searchControl.value.text,
      domain: undefined
    };

    // operations
    let index = [...this.searchChips].reverse().findIndex(chip => chip.fieldname === newChip.fieldname); // find last to first
    const and = newChip.text.startsWith('&'); // check if & operator found to make an AND
    newChip.text = and ? newChip.text.substring(1) : newChip.text; // if 'and' delete operator

    // domain
    const domain = `('${
      metafield.relation === 'many2one' ? newChip.fieldname + '__str_name' : newChip.fieldname
      }','icontains','${newChip.text}')`; // new domain



    // handle newChip
    if (index >= 0) { // existing chip found
      index = this.searchChips.length - 1 - index; // return reversed index to original index
      if (keyref === 'enter' && !and) { // To make an OR domain
        this.searchChips[index] = {
          label: newChip.label,
          fieldname: newChip.fieldname,
          text: `${this.searchChips[index].text} | ${newChip.text}`,
          domain: `${this.searchChips[index].domain},'|',${domain}`
        };
      } else { // To make an AND domain
        // insert after last index (with same newChip.field) to keep order presentable to user
        this.searchChips.splice(index + 1, 0, { ...newChip, domain });
      }

    } else { // no chip found
      this.searchChips.push({ ...newChip, domain });
    }

    // EVENT
    this.domainEvent.emit({ chips: this.searchChips, domain: this.computeDomain() }); // trigger output event
  }

  /** only emit event when autocomplete is open */
  press(keyref: string) {
    if (this.matAutocomplete.isOpen) { this.replaySub.next(keyref); } // emit value
  }

  remove(index: number): void {
    this.searchChips.splice(index, 1); // remove chip
    this.domainEvent.emit({ chips: this.searchChips, domain: this.computeDomain() }); // trigger output event
  }

  chipPressed(index: number) {
    if (userConfirm('Desea eliminar este filtro?')) {
      this.remove(index);
    }
  }


  // DOMAIN

  computeDomain(): string {
    // map each chip domain (with ot without brackets)
    const mappedOR = this.searchChips.map(chip => chip.domain.split('|').length > 1 ? `[${chip.domain}]` : chip.domain);
    // domain array joined with AND, if array have elements
    const joinedAND = mappedOR.length > 0 ? `[${mappedOR.join(`,'^',`)}]` : '';
    // return
    return joinedAND;
  }

  // DESTROY

  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }
}
