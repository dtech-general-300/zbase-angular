import { Component, Input, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';


/** avoid inncesary code computing whe change detection runs */
@Component({
  selector: 'app-master-check',
  template: `
    <ng-container *ngIf="{ len: length$ | async, count: count$ | async } as data">
      <mat-checkbox
      (change)="masterToggle(data.count!==data.len)"
      [checked]="data.count > 0 && data.count === data.len"
      [indeterminate]="data.count > 0 && data.count !== data.len"
      [disabled]="disbld"></mat-checkbox>
    </ng-container>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MasterCheckComponent {

  @Input() disbld: boolean;
  @Input() length$: Observable<any>;
  @Input() count$: Observable<any>;
  @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  // /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(newValue: boolean) {
    this.toggle.emit(newValue);
  }

}


/** when change detection run, this component avoid innecesary computations, improving the perfomarnce */
@Component({
  selector: 'app-check',
  template: '<mat-checkbox (change)="singleToggle($event)" [checked]="check?.selected" [disabled]="disbld"></mat-checkbox>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckComponent {

  @Input() disbld: boolean;
  @Input() check: any; // actualiza el valor del checkbox
  @Output() toggle: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  /** cambia solo el checkbox seleccionada */
  singleToggle(value: boolean) {
    this.toggle.emit(value);
  }

}

