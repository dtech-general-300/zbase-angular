import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Many2oneComponent } from './many2one.component';

describe('Many2oneComponent', () => {
  let component: Many2oneComponent;
  let fixture: ComponentFixture<Many2oneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Many2oneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Many2oneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
