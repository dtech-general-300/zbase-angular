import {
  Component, OnInit, ChangeDetectionStrategy, Input,
  Output, EventEmitter, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef, Inject
} from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Subject, of, timer } from 'rxjs';
import { takeUntil, switchMap, map, debounce } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { GenericQuery } from '../../state/generic.query';
import { ModelHttp } from 'src/app/core/xservices';
import { MatAutocomplete, MatAutocompleteTrigger } from '@angular/material';
import { FocusMonitor } from '@angular/cdk/a11y';
import { maingq } from '../../state/generic.token';


@Component({
  selector: 'app-many2one',
  templateUrl: './many2one.component.html',
  styleUrls: ['./many2one.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Many2oneComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  @Input() type: 'normal' | 'inlist' = 'normal';
  @Input() parent: FormGroup; // parent formGroup reference
  @Input() meta: any; // field metadata received from server
  @Input() validators: any; // validators for ficticious field m2o group
  @Input() error: any; // error tuple [bool, message]
  @Input() edit: boolean; // if inlist, the state of editing the field

  @Output() keys = new EventEmitter<string>();
  @ViewChild('pFocus', { static: false }) pFocus: ElementRef<HTMLElement>;
  @ViewChild('auto', { static: false }) auto: MatAutocomplete;
  @ViewChild('trigger', { static: false }) trigger: MatAutocompleteTrigger;

  // sub emit a tuple: tuple[0] is a flag (allow to query values only when necesary); tuple[1] is debounceTime
  sub = new Subject<[false | string, number]>();
  sub$ = this.sub.asObservable();

  mode: string; // form Mode: edit, create, read
  m2o: FormGroup; // ficticious field group that allow to work with many2one fields
  realm2o: FormControl; // the real m2o field that references to parent
  filteredOptions: any[]; // contains the options of autocomplete
  isFocused = false; // whether the input is focused or not
  isLoading = false; // allow to null the flickering effect of autoActiveFirstOption
  isEditing = false; // whether the field has valid data or is editing
  lastValue = null; // cache last value (works toghether with isEditing)

  emptyVal = { id: null, str_name: '' };

  constructor(
    @Inject(maingq) private gQuery: GenericQuery,
    private fb: FormBuilder,
    private focusMonitor: FocusMonitor,
    private route: ActivatedRoute,
    private mHttp: ModelHttp,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    // set m2o only once
    const strName = this.validators.length > 1 ? ['', this.validators[1]] : [''];
    this.m2o = this.fb.group({ id: [null], str_name: strName });

    // data change
    this.route.data.pipe(takeUntil(this.u$)).subscribe(() => { // after each resolver (the data change)
      this.lastValue = null; // allow to track "first lastValue" in onFocus Method
      this.realm2o = this.parent.controls[this.meta.fieldname] as FormControl; // get real field
      this.syncField(); // set ficticious field
    });

    // mode change
    this.gQuery.formMode$.pipe(takeUntil(this.u$)).subscribe(mode => {
      this.mode = mode;
      if (mode === 'read') { this.lastValue = null; } // allow to clean value when mode is read
      this.syncField(); // set ficticious field
      this.cdr.markForCheck();
    });

    // input change
    this.m2o.controls.str_name.valueChanges.pipe(takeUntil(this.u$)).subscribe(value => {
      if (this.mode !== 'read' && this.isFocused) {
        this.isEditing = true;
        if (value === '') { this.selected({ value: this.emptyVal }); } // this means null to backend (override isEditing to false)
        this.sub.next(['typing', 420]); // flag with debounceTime value for typing (override sub.next emitted in selected method)
      }
    });

    // realm2o change (in inlist type, this run when enable/disable of this control is called too)
    this.realm2o.valueChanges.pipe(takeUntil(this.u$)).subscribe(() => {
      this.syncField();
    });

    // flag change (sub emitF)
    this.sub$.pipe(
      debounce(([_, time]) => timer(time)),
      switchMap(([flag, _]) => {
        if (flag) {
          let value = '';
          switch (flag) {
            case 'typing':
            case 'opened': value = this.m2o.controls.str_name.value; break;
            case 'toggled': if (this.isEditing) { value = this.m2o.controls.str_name.value; } break;
          }
          this.trigger.openPanel(); // ensure isOpen works fine
          return this.mHttp.fetchList(this.meta.api_route, `[('str_name','icontains','${value}')]`, `['str_name']`, 7).pipe(
            map(response => response.results)
          );
        }
        this.trigger.closePanel(); // ensure isOpen works fine
        return of([]);
      }),
      takeUntil(this.u$) // allways takeUnitl goes at the last position in pipe()
    ).subscribe(data => {
      this.filteredOptions = data;
      this.isLoading = true;
      this.cdr.markForCheck();
    });
  }

  syncField() {
    // set field value
    const value = this.realm2o.value === null ? this.emptyVal : this.realm2o.value;
    this.m2o.patchValue(value, { emitEvent: false }); // sync data: real to m2o
    // set field status
    this.syncEnable();
  }

  syncEnable() { // synchro real enable with ficticious enable
    if (this.realm2o.enabled) {
      if (this.m2o.disabled) { this.m2o.enable({ emitEvent: false }); }
    } else {
      if (this.m2o.enabled) { this.m2o.disable({ emitEvent: false }); }
    }
  }

  // Use this method when widget control in ui is not the original (real) subgroup, just a help for the original.
  // This method is SURE that only runs when mode !== read because that is programmed in form.component
  // but the var this.mode is still 'read' because the obsevable formMode$ dont emit it yet
  internalFocus() {
    if (this.realm2o.enabled) {
      this.m2o.enable({ emitEvent: false }); // enable not real control to allow enable (is util when type==='normal')
      this.focusMonitor.focusVia(this.pFocus, 'program'); // focus enabled control
    }
  }




  // AVOID autoActiveFirstOption Flickering Effect
  // allow to put a scss class on first item until the data has arrived
  avoidFlicker(index: number) {
    if ((index === this.filteredOptions.length - 1) && this.isLoading) {
      setTimeout(() => { this.isLoading = false; }, 30);
    }
    return index === 0 && this.isLoading;
  }



  // AUTOCOMPLETE OVERRIDE BEHAVIOUR
  // allow to control selection with mouse and keyborad at the same time

  over(index: number) {
    if (!this.isLoading) { // to avoid flicker effect
      this.auto._keyManager.setActiveItem(index); // this is hacky (keymanager is private)
    }
  }

  leave() {
    this.auto._keyManager.setActiveItem(null); // this is hacky (keymanager is private)
  }



  // NORMAL METHODS

  onFocus() {
    // track the "first lastValue"
    if (this.lastValue === null) { this.lastValue = { ...this.m2o }; }
  }

  forceClose() { // use it when leave this control with a key or focus other programatically, etc.
    this.sub.next([false, 0]); // force close (the same as panel('close') without condition isOpen)
  }

  exit() {
    // control coherence when leave the control
    if (this.isEditing) {
      if (this.lastValue !== null) { // not null avoid the mode==='read' error which make lastValue null
        this.selected(this.lastValue);
      }
    }
  }

  selected(option: any) {
    // set value but avoid event input change
    this.m2o.patchValue(option.value, { emitEvent: false });

    // set real field value (if emptyValue has been assigned to m2o, id will be null)
    const value = this.m2o.value.id === null ? null : this.m2o.value;
    this.realm2o.setValue(value, { emitEvent: false }); // sync data: m2o to real (like a save method)
    this.realm2o.markAsDirty(); // because a option was selected

    // reset flags & close autoComplete
    this.isEditing = false;
    this.lastValue = { ...this.m2o };
    this.sub.next([false, 0]);
  }

  panel(action: string) {
    if (this.mode !== 'read') {
      switch (action) {
        case 'toggle': this.sub.next([this.auto.isOpen ? false : 'toggled', 0]); break; // always emits the isOpen opposite
        case 'open': if (!this.auto.isOpen) { this.sub.next(['opened', 0]); } break; // make a request if not Open
        case 'close': if (this.auto.isOpen) { this.sub.next([false, 0]); } break; // no need to emit when isOpen is false
      }
    }
  }


  // keys

  keyEsc() { // only for inlist widget
    if (this.auto.isOpen) {
      this.panel('close');
    } else {
      this.forceClose();
      this.keys.emit('esc');
    }
  }

  keyEnter() { // only for inlist widget
    if (!this.auto.isOpen && !this.isEditing) {
      this.forceClose();
      this.keys.emit('enter');
    }
  }

  keyShiftEnter() { // only for inlist widget
    if (!this.auto.isOpen && !this.isEditing) {
      this.forceClose();
      this.keys.emit('shift+enter');
    }
  }

  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }
}
