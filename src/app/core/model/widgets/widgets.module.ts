import { NgModule } from '@angular/core';
import { One2manyComponent } from './one2many/one2many.component';
import { ReactFormModule } from '../../xshared';
import { CharComponent } from './char/char.component';
import { IntegerComponent } from './integer/integer.component';
import { Many2oneComponent } from './many2one/many2one.component';
import { Many2manyComponent } from './many2many/many2many.component';
import { M2mPopupComponent } from './many2many/m2m-popup/m2m-popup.component';
import { MasterCheckComponent, CheckComponent } from './base/list/check/check.component';
import { FilterComponent } from './base/list/filter/filter.component';
import { ListtableComponent } from './base/list/listtable/listtable.component';
import { PaginatorComponent } from './base/list/paginator/paginator.component';



@NgModule({
  declarations: [
    // base
    MasterCheckComponent,
    CheckComponent,
    FilterComponent,
    ListtableComponent,
    PaginatorComponent,
    // widgets
    One2manyComponent,
    Many2manyComponent,
    Many2oneComponent,
    CharComponent,
    IntegerComponent,
    M2mPopupComponent,
  ],
  imports: [
    ReactFormModule,
  ],
  exports: [
    // base
    MasterCheckComponent,
    CheckComponent,
    FilterComponent,
    ListtableComponent,
    PaginatorComponent,
    // widgets
    One2manyComponent,
    Many2manyComponent,
    Many2oneComponent,
    CharComponent,
    IntegerComponent
  ],
  entryComponents: [
    M2mPopupComponent
  ],
})
export class WidgetsModule { }
