import {
  Component, OnInit, Input, ChangeDetectionStrategy, OnDestroy,
  Output, EventEmitter, ChangeDetectorRef, ViewChild, Inject
} from '@angular/core';
import { FormGroup, FormArray } from '@angular/forms';
import { Subject } from 'rxjs';
import { GenericQuery } from '../../state/generic.query';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { M2mPopupComponent } from './m2m-popup/m2m-popup.component';
import { ListtableComponent } from '../base/list/listtable/listtable.component';
import { maingq } from '../../state/generic.token';
import { ValueConverter } from '@angular/compiler/src/render3/view/template';



@Component({
  selector: 'app-many2many',
  templateUrl: './many2many.component.html',
  styleUrls: ['./many2many.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class Many2manyComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  @Input() parent: FormGroup; // parent formGroup reference
  @Input() meta: any; // field metadata received from server
  @Input() def: { [key: string]: any }; // definition of field
  @Output() ready: EventEmitter<boolean> = new EventEmitter<boolean>();

  mode: string; // form Mode: edit, create, read

  // for listtable
  @ViewChild('listtable', { static: false }) listtable: ListtableComponent; // to get listtable variables
  config: any; // general config
  group: FormGroup;
  columnNames: string[]; // array of fieldnames in order to compose mat-table



  constructor(
    @Inject(maingq) private gQuery: GenericQuery,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {
    // list
    this.columnNames = this.meta.fields.map(field => field.fieldname);
    this.route.data.pipe(takeUntil(this.u$)).subscribe(() => { // after each resolver
      this.syncGroup();
    });

    // mode change
    this.gQuery.formMode$.pipe(takeUntil(this.u$)).subscribe(mode => {
      this.mode = mode;
      this.syncGroup();
    });

    // for listtable
    this.config = {
      def: this.def,
      selection: 'no',
      deletion: true,
      noClickable: true, // force cells to be no clickable
      bclick: true, // force buttons to emit when add(), delete(), etc.
      customName: this.meta.fieldname,
      style: {
        headerRowSticky: true,
        elevation: 'mat-elevation-z0',
        addButtonName: 'Agregar/Quitar'
      }
    };
  }

  syncGroup() {
    this.group = { ...this.parent } as FormGroup;
    this.cdr.markForCheck(); // allow to update because of OnPUsh
  }


  // TABLE & Update

  bclick(button: string) {
    switch (button) {
      case 'add': this.add(); break;
      case 'delete': this.delete(); break;
    }
  }

  add() {
    console.log();
    // ids
    const array = this.group.controls[this.meta.fieldname].value;
    const ids = array.length > 0 ? array.map(c => c.id) : [];

    const dialogRef = this.dialog.open(M2mPopupComponent, {
      position: { top: '21px' },
      maxWidth: '980px',
      width: '91%',
      panelClass: 'custom-dialog-container',
      data: {
        meta: this.meta,
        currentIds: ids,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === undefined) { // cancel pressed, clickout or X
        console.log('cancel');
      } else {
        console.log('ok');
        console.log(result);
        // this.mH
        (this.group.controls[this.meta.fieldname] as FormArray).setValue();
      }
    });
  }

  delete() {

  }

  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }
}
