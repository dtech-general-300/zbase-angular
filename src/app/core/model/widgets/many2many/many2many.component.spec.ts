import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Many2manyComponent } from './many2many.component';

describe('Many2manyComponent', () => {
  let component: Many2manyComponent;
  let fixture: ComponentFixture<Many2manyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Many2manyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Many2manyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
