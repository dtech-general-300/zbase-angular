import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { M2mPopupComponent } from './m2m-popup.component';

describe('M2mPopupComponent', () => {
  let component: M2mPopupComponent;
  let fixture: ComponentFixture<M2mPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ M2mPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(M2mPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
