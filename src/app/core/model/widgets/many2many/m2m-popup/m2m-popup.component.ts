import {
  Component, Inject, OnInit, ChangeDetectionStrategy,
  OnDestroy, ViewChild, ViewContainerRef, ComponentRef, ChangeDetectorRef
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { popupToken, popupgs, popupgq } from '../../../state/generic.token';
import { ModelHttp, DynamicThemeService } from 'src/app/core/xservices';
import { from, Observable, Subject, BehaviorSubject } from 'rxjs';
import { MenuService, MenuQuery } from 'src/app/core/xstate';
import { GenericQuery } from '../../../state/generic.query';
import { GenericService } from '../../../state/generic.service';
import { FormGroup } from '@angular/forms';
import { ListtableComponent } from '../../base/list/listtable/listtable.component';
import { Chips } from '../../../state/generic.store';
import { LazyLoaderService } from 'src/app/core/xconfig';
import { takeUntil, skip } from 'rxjs/operators';
import { FilterComponent } from '../../base/list/filter/filter.component';

@Component({
  selector: 'app-m2m-popup',
  templateUrl: './m2m-popup.component.html',
  styleUrls: ['./m2m-popup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [popupToken]
})
export class M2mPopupComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  // themes
  theme$: Observable<number>; // the observable for themes

  // list view
  @ViewChild('host', { read: ViewContainerRef, static: true }) host: ViewContainerRef; // dynamic components
  compRef: ComponentRef<any> = null;

  group: FormGroup;
  count$: Observable<number>; // qty of selected records
  columnNames$: Observable<string[]>; // displayed columns, array of fieldnames
  metaColumns$: Observable<any[]>; // list metadata
  empty: boolean; // if store is empty (record count === 0)

  // listtable
  @ViewChild('filter', { static: false }) filter: FilterComponent; // to get listtable variables
  @ViewChild('listtable', { static: false }) listtable: ListtableComponent; // to get listtable variables
  check: any; // config for checkboxes (to put in listtable)
  config: any; // general config (to put in listtable)

  // filter
  chips$: Observable<Chips[]>; // array of chips from store
  view: any; // metadata of the server view

  // pagination
  pagination$: Observable<string[]>;

  // resolvers
  action: any; // the current popup action
  compFactory: any; // component Factory
  dataSub = new BehaviorSubject<any>(null); // allow to simulate route.data
  data$ = this.dataSub.asObservable(); // allow to simulate route.data
  resolving = true; // if component is loading for first time

  // ids
  ids: number[]; // array of new ids (to return to control)


  constructor(
    // dialog
    public dialogRef: MatDialogRef<M2mPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    // other
    private menuService: MenuService,
    private menuQuery: MenuQuery,
    private mHttp: ModelHttp,
    private lazyLoader: LazyLoaderService,
    private cdr: ChangeDetectorRef,
    private dynamic: DynamicThemeService,
    // tokens
    @Inject(popupgq) private pgQuery: GenericQuery,
    @Inject(popupgs) private pgService: GenericService,
  ) { }

  async ngOnInit() {
    // theme
    this.theme$ = this.dynamic.theme$; // get the observable from service

    // initial ids
    this.ids = this.data.currentIds;

    // intial resolvers
    // setTimeout: because "cargando" provoke ExpressionChangedAfterItHasBeenCheckedError // TODO: handle errors
    setTimeout(async () => {
      await this.resolverAction();
      await this.resolveData();
    }, 0);

    // observables
    this.count$ = this.pgQuery.getSelectCount(); // cantidad de seleccionados
    this.pagination$ = this.pgQuery.pagination$; // pagination data
    this.chips$ = this.pgQuery.chips$; // filter

    // for listtable
    this.metaColumns$ = this.pgQuery.columns$; // all columns data
    this.columnNames$ = this.pgQuery.columnNames$; // column names (fieldnames)

    // run after resolvers
    this.data$.pipe(skip(1), takeUntil(this.u$)).subscribe(() => {
      // run only once or when model change
      if (!this.compRef) {
        // instantiate
        this.host.clear(); // clear all content of host
        this.compRef = this.host.createComponent(this.compFactory); // instantiate list component
        this.compRef.instance.init(popupgq);
        // cdr
        this.resolving = false;
        this.cdr.markForCheck();
      }
      // run always
      this.setConfig();
      this.syncList();
      this.empty = this.pgQuery.getEmpty();
    });
  }


  // RESOLVER
  async resolverAction() {
    this.action = this.menuQuery.getActionByRef(this.data.meta.popup_action); // return the action (from store) or false
    if (!this.action) { // if not in store call from api
      const aData = await from(this.menuService.getActionByRef(this.data.meta.popup_action)).toPromise();
      this.action = aData[0];
    }
    this.view = this.menuQuery.getView(this.action.id, 0, 'list');

    // load factory
    this.compFactory = await this.lazyLoader.loadAndGetCompFactory(this.view.arguments.list_path, 'list');
  }

  async resolveData(limit: any = '', offset = 0) {
    // get data
    const listData = await from(this.mHttp.fetchList(
      this.view.arguments.api_route, // server route
      this.pgQuery.getFilterDomain(), // query domain
      '', // order
      limit,
      offset
    )).toPromise();

    // set data in store
    this.pgService.updateListArch(this.view, this.menuQuery.getModelName(this.action.id, 0)); // here we update ModelName
    this.pgService.setList(listData.results);
    this.pgService.updatePaginator(listData);

    // update Ids
    this.pgService.updateSelection(this.ids, true); // for keep tracking the selected ones

    // emit
    this.dataSub.next(listData);
  }


  // LISTTABLE

  setConfig() {
    this.check = {
      len$: this.pgQuery.selectCount(), // longitud de toda la colleccion de entidades VISIBLES
      count$: this.count$, // conteo de las entidades que tienen check activado
      checks: this.pgQuery.selectSelected(), // observable de un objeto que tiene ui entities con el valor de cada check
    };
    this.config = {
      def: this.compRef.instance.definition,
      selection: 'multi',
      style: {
        headerRowSticky: true,
        footerRowSticky: true,
        elevation: 'mat-elevation-z0'
      }
    };
  }

  syncList() {
    this.compRef.instance.syncAll(this.pgQuery.getAll());
    this.group = { ...this.compRef.instance.group }; // this group contains the List in a FormArray
  }


  // CHECKS

  masterCheck(value: boolean) {
    this.pgService.updateSelection(null, value); // null cambia a todos
  }

  singleCheck(single: any) {
    // select
    this.pgService.updateSelection(single.id, !this.pgQuery.getSelected(single.id));
    const index = this.ids.indexOf(single.id);
    if (index > -1) {
      this.ids.splice(index, 1); // delete if exist
    } else {
      this.ids.push(single.id); // add if not exist
    }

    // focus (to improve UX)
    this.filter.focus();
  }


  // PAGINATOR

  goPage(where: string) { // TODO: when press next or prev and records have been deleted just go to page 1 and alert user.
    const pag = this.pgQuery.getPaginator();
    if (pag.enabled) { // avoid navigate if not necesary
      this.resolveData(pag.limit, pag[where]);
    } else {
      this.filter.focus();
    }
  }

  customPage(values: any) { // go to custom page (custom means custom pagination)
    this.resolveData(values.end - values.start + 1, values.start - 1);
  }

  // FILTER

  domain(result: any) { // TODO: filter by selected or not selected
    this.pgService.setChips(result); // set updated array of chips, this also set filterDomain
    const pag = this.pgQuery.getPaginator();
    this.resolveData(pag.limit, 0);
  }


  // dialog

  cancel() {
    this.dialogRef.close();
  }


  // DESTROY
  ngOnDestroy() {
    this.pgService.destroy();
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }

}
