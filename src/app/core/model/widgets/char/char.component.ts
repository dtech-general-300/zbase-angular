import { Component, OnInit, Input, ViewChild, ElementRef, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-char',
  templateUrl: './char.component.html',
  styleUrls: ['./char.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CharComponent implements OnInit {

  @Input() type: 'normal' | 'inlist' = 'normal';
  @Input() parent: FormGroup; // parent formGroup reference
  @Input() meta: any; // field metadata received from server
  @Input() error: any; // error tuple [bool, message]
  @Input() edit: boolean; // if inlist, the state of editing the field

  @Output() keys = new EventEmitter<string>();
  @ViewChild('focus', { static: false }) focus: ElementRef<HTMLElement>;


  constructor() { }

  ngOnInit() {
  }

}
