import {
  Component, OnInit, OnDestroy,
  ViewContainerRef, ComponentRef, ViewChild, ChangeDetectorRef, Inject
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, from, of } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { userConfirm } from '../../xguards';
import { MenuQuery, BreadCrumb, GeneralQuery, MenuService } from '../../xstate';
import { GenericQuery } from '../state/generic.query';
import { ModelHttp } from '../../xservices/model.http';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { GenericService } from '../state/generic.service';
import { Chips } from '../state/generic.store';
import { FormGroup, AbstractControl, FormBuilder } from '@angular/forms';
import { ListtableComponent } from '../widgets/base/list/listtable/listtable.component';
import { maingq, maings } from '../state/generic.token';


/** TODO: responsive list */
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  // list view
  @ViewChild('host', { read: ViewContainerRef, static: true }) host: ViewContainerRef; // dynamic components
  compRef: ComponentRef<any> = null;
  prevModel: string; // a flag to know if model changed

  group: FormGroup;
  count$: Observable<number>; // qty of selected records
  columnNames$: Observable<string[]>; // displayed columns, array of fieldnames
  metaColumns$: Observable<any[]>; // list metadata
  empty: boolean; // if store is empty (record count === 0)
  newMessage: string; // message showed when empty === true

  // pagination
  pagination$: Observable<string[]>;
  data$: Observable<any>; // to indicate data is ready

  // listtable
  @ViewChild('listtable', { static: false }) listtable: ListtableComponent; // to get listtable variables
  check: any; // config for checkboxes (to put in listtable)
  config: any; // general config (to put in listtable)
  editable: any; // general config (to put in listtable)


  // filter
  chips$: Observable<Chips[]>; // array of chips from store
  view: any; // metadata of the server view

  // bread
  breadcrumbs$: Observable<BreadCrumb[]>;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(maingq) private gQuery: GenericQuery,
    @Inject(maings) private gService: GenericService,
    private modelHttp: ModelHttp,
    private menuQuery: MenuQuery,
    private menuService: MenuService,
    private generalQuery: GeneralQuery,
    private matIconRegistry: MatIconRegistry,
    private cdr: ChangeDetectorRef,
    private domSanitizer: DomSanitizer
  ) {
    // registry svg that can be used in html with mat-icon. Never use untrusted svg or url in the method
    // bypassSecurityTrustResourceUrl cause this skip security (that because globally security is activated)
    this.matIconRegistry.addSvgIcon(
      'newFile',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../assets/svg/smiling_file.svg')
    );
  }

  ngOnInit() {

    // observables
    this.count$ = this.gQuery.getSelectCount(); // cantidad de seleccionados
    this.breadcrumbs$ = this.generalQuery.breadcrumbs$; // breadcrumb array

    // for pagination
    this.pagination$ = this.gQuery.pagination$; // pagination data
    this.data$ = this.route.data;

    // filter
    this.chips$ = this.gQuery.chips$;

    // for listtable
    this.metaColumns$ = this.gQuery.columns$; // all columns data
    this.columnNames$ = this.gQuery.columnNames$; // column names (fieldnames)


    // run after resolvers
    this.route.data.pipe(takeUntil(this.u$)).subscribe(data => {
      // run only once or when model change
      if (!this.compRef || this.prevModel !== this.gQuery.getValue().gui.model) {
        this.prevModel = this.gQuery.getValue().gui.model;
        this.host.clear(); // clear all content of host
        this.compRef = this.host.createComponent(data.list.compFactory); // instantiate list component
        this.compRef.instance.init(); // always call after instantation
      }
      // run always
      this.setConfig();
      this.syncList();
      this.empty = this.gQuery.getEmpty();
    });
  }

  setConfig() {
    // for filter and view for otherthings
    this.view = this.menuQuery.getSingleView('list');

    // simple vars
    this.newMessage = this.view.arguments.new;

    // for listtable
    this.editable = this.view.arguments.editable ? this.view.arguments.editable : false;
    this.check = {
      len$: this.gQuery.selectCount(), // longitud de toda la colleccion de entidades VISIBLES
      count$: this.count$, // conteo de las entidades que tienen check activado
      checks: this.gQuery.selectSelected(), // observable de un objeto que tiene ui entities con el valor de cada check
    };
    this.config = {
      def: this.compRef.instance.definition,
      selection: 'multi',
      deletion: false,
      clickOut: true,
      escFunc: async (o) => await this.cancel(o),
      saveFunc: async (o) => await this.save(o),
      externalCreate: true,
      style: {
        headerRowSticky: true,
        elevation: 'mat-elevation-z4'
      }
    };
  }


  // BREADCRUMBS

  breadCrumbs(cRoute: string) {
    this.router.navigate([cRoute], { relativeTo: this.route.parent.parent.parent }); // TODO: not tested
  }

  // FORM

  form(row: any) {
    this.router.navigate([row.rowRecord.id], { relativeTo: this.route });
  }

  customDeactivate(control: AbstractControl) { // ask user if he want to discard changes
    let canDeactivate = true;
    if (control.dirty) { // si los fields fueron modificados
      canDeactivate = userConfirm('Desea descartar los cambios?');
    }
    return canDeactivate;
  }

  create() {
    if (this.editable) {
      if (this.empty) { this.empty = false; this.cdr.detectChanges(); } // allow that listtable appears in DOM (to use this.listtable.add)
      this.listtable.add();
    } else {
      this.router.navigate(['new'], { relativeTo: this.route });
    }
  }


  // TODO DECORATOR TO FRONTEND VALIDation
  async save(out: any) {
    if (out.clickOut && out.record) { // solo si fue emitido con clickOut true y un record esta en modo edicion

      // delete if pristine
      if (this.listtable.creating && out.record.pristine) {
        this.listtable.disableAny();
        return true;
      }

      // save
      let res$: Observable<any>;
      const modelRoute = this.menuQuery.getSingleView('list').arguments.api_route;
      const id = out.record.value.id;

      if (this.listtable.creating) {
        res$ = this.modelHttp.save(modelRoute, out.record.getRawValue()); // raw values of form (included disabled)
      } else {
        res$ = out.record.dirty ? this.modelHttp.modify(modelRoute, id, out.record.getRawValue()) : of(this.gQuery.getEntity(id));
      }

      return await new Promise(resolve => {
        res$.subscribe(
          resRecord => {

            this.listtable.disableAny(); // deshabilitar el row actual
            if (!out.record.get('id')) { // añadir id a la estructura si es necesario (cuando se crea)
              this.listtable.data.setControl(out.index, this.fb.group({
                id: [null], to_many: ['created'], ...this.config.def
              }));
              this.updatePaginator(); // allow to update end (only 1 time), total of records, and enable arrows
            }

            this.compRef.instance.syncSingle(this.listtable.data.at(out.index), resRecord); // actualizar datos a la estructura
            out.record.markAsPristine({ onlySelf: true }); // set pristine
            this.gService.upsertRecord(resRecord.id, resRecord); // update store
            this.listtable.syncList(); // allow to update the list displayed in listtable (avoid checks problems and maybe others)
            resolve(true);
          },
          () => { resolve(false); } // TODO: handle errors
        );
      });
    }
  }

  updatePaginator() {
    // paginator update
    const paginator = { ...this.gQuery.getPaginator() };
    if (paginator.count === 0) { paginator.end += 1; }
    paginator.count += 1;
    paginator.enabled = true;
    this.gService.setPaginator(paginator);
  }

  emode(state: string) { // allow to forbbid click in menus on toolbar o sidenav when editing a record (avoid inconsistencies)
    this.menuService.canFlag(state === 'start' ? false : true);
  }

  async cancel(out: any = null) {
    // get the current editing row (use this when you want to clickousite in other button or something)
    if (out === null) {
      this.config.clickOut = false;
      out = await from(this.listtable.out.pipe(take(1))).toPromise();
      this.config.clickOut = true;
    }

    // change to original data
    const response = this.customDeactivate(out.record);
    if (response) { // only ask when record was modified, true directly if not modified
      // disable
      if (this.listtable.creating) { out.record.markAsPristine({ onlySelf: true }); } // allow to delete new, after cancel it
      this.listtable.disableAny(); // disable the record and mark parent as dirty if necesary

      // if the record was new and was deleted the following methods preactically don't do nothing

      // set original data
      const recordData = this.gQuery.getEntity(out.record.value.id);
      this.compRef.instance.syncSingle(out.record, recordData);
      // allow to mantain controls marked as pristine but the "group" and "group.listArray" as dirty if modified
      out.record.markAsPristine({ onlySelf: true });
    }

    return response; // finish await of the caller of this function
  }



  // LIST

  goPage(where: string) { // TODO: when press next or prev and records have been deleted just go to page 1 and alert user.
    const pag = this.gQuery.getPaginator();
    if (pag.enabled) { // avoid navigate if not necesary
      this.router.navigate(['./'], { relativeTo: this.route, state: { limit: pag.limit, offset: pag[where] } });
    }
  }

  customPage(values: any) { // go to custom page (custom means custom pagination)
    this.router.navigate(['./'], { // TODO: show progress bar when start to navigate. Handle errors of responses
      relativeTo: this.route,
      state: {
        limit: values.end - values.start + 1,
        offset: values.start - 1
      }
    });
  }

  delete() {
    if (userConfirm('Está seguro que desea eliminar estos registros?')) {
      const ids = this.gQuery.getSelectedIds();
      const modelRoute = this.menuQuery.getSingleView('list').arguments.api_route;

      // delete ids in current model apiRoute
      this.modelHttp.deleteIds(modelRoute, ids).subscribe(data => {
        this.router.navigate(['./'], { relativeTo: this.route });
        // TODO: update with the same filters, order, and paginations (for pagination conserve offset but limit - deleted)
        // TODO: maybe is better just to delete from store and not navigate to request again the updated data
        // or maybe it is ok with actual behaviour
      });
    }
  }

  syncList() {
    this.compRef.instance.syncAll(this.gQuery.getAll());
    this.group = { ...this.compRef.instance.group }; // this group contains the List in a FormArray
  }

  // FILTER
  domain(result: any) {
    this.gService.setChips(result); /// set updated array of chips, this also set filterDomain
    const pag = this.gQuery.getPaginator();
    this.router.navigate(['./'], { relativeTo: this.route, state: { limit: pag.limit, offset: 0 } });
  }

  // CHECKBOX
  masterCheck(value: boolean) {
    this.gService.updateSelection(null, value); // null cambia a todos
  }

  singleCheck(single: any) {
    this.gService.updateSelection(single.id, !this.gQuery.getSelected(single.id));
  }



  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }

}

