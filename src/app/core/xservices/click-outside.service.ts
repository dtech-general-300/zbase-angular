import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


/**
 * this service provide a global subject to keep track last target clicked
 * element inside the document, this target is emitted in appcomponent and
 * used inside ClickOutside Directive
 */
@Injectable({
  providedIn: 'root'
})
export class ClickOutsideService {

  documentClickedTarget: Subject<HTMLElement> = new Subject<HTMLElement>();
  target$ = this.documentClickedTarget.asObservable();

  constructor() { }

}
