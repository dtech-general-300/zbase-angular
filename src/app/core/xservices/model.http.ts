import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { baseURL, StandardInterception } from '../xconfig';


/**
 * this service is a singleton http-generic service
 */
@Injectable({
  providedIn: 'root'
})
export class ModelHttp {

  constructor(
    private http: HttpClient
  ) { }

  /** fetch records with paginations TODO: query param to return only needed columns (fields) */
  fetchList(modelRoute: string, query = '', order = '', limit: string | number = '', offset: number = 0): Observable<any> {

    query = query !== '' ? `query=${query}&` : ''; // format query to request
    order = order !== '' ? `order=${order}&` : ''; // format order to request

    // request
    const res$ = this.http.get<any>(
      `${baseURL + modelRoute}?${query + order}limit=${limit}&offset=${offset}`, // limit='': use server default_limit
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    // res$.subscribe(
    //   data => { console.log(); },
    //   () => { }
    // );

    return res$;
  }

  /** fetch single record TODO: query param to return only needed columns (fields) */
  fetchRecord(modelRoute: string, id: number): Observable<any> {
    // request
    const res$ = this.http.get<any>(
      `${baseURL + modelRoute + id}/`,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    // res$.subscribe(
    //   data => { console.log(data); },
    //   () => { }
    // );

    return res$;
  }

  save(modelRoute: string, object: any): Observable<any> {
    // request
    const res$ = this.http.post<any>(
      baseURL + modelRoute,
      object,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    // res$.subscribe(
    //   data => { console.log(data); },
    //   () => { }
    // );

    return res$;
  }

  modify(modelRoute: string, id: number, object: any): Observable<any> {
    // request
    const res$ = this.http.patch<any>(
      `${baseURL + modelRoute + id}/`,
      object,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    // res$.subscribe(
    //   data => { console.log(data); },
    //   () => { }
    // );

    return res$;
  }

  delete(modelRoute: string, id: number): Observable<any> {
    const res$ = this.http.delete<any>(
      `${baseURL + modelRoute + id}/`,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    // res$.subscribe(
    //   data => { console.log(data); },
    //   () => { }
    // );

    return res$;
  }

  deleteIds(modelRoute: string, ids: number[]) {
    const res$ = this.http.delete(
      `${baseURL + modelRoute}destroy_many/?query=[('id','in',[${ids.join(',')}])]`,
      { headers: StandardInterception }
    ).pipe(shareReplay(1));

    // res$.subscribe(
    //   data => { console.log(data); },
    //   () => { }
    // );

    return res$;
  }





  // create<T>(path: string, object: T | any): Observable<T | T[]> {
  //   return this.http.post<T | T[]>(
  //     baseURL + path,
  //     object,
  //     { headers: StandardInterception }
  //   );
  // }

  // read<T>(path: string): Observable<T | T[]> {
  //   return this.http.get<T | T[]>(
  //     baseURL + path,
  //     { headers: StandardInterception }
  //   );
  // }

  // update<T>(path: string, object: T | any): Observable<T | T[]> {
  //   return this.http.patch<T | T[]>(
  //     baseURL + path + '/' + object.id,
  //     object,
  //     { headers: StandardInterception }
  //   );
  // }

  // delete<T>(path: string, object: T | any): Observable<T | T[]> {
  //   return this.http.delete<T | T[]>(
  //     baseURL + path + '/' + object.id,
  //     { headers: StandardInterception }
  //   );
  // }

}
