import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Meta } from '@angular/platform-browser';


// this service make possible to change the theme
// and change the browser color in mobile, also has
// the ability to ser a default theme in initTheme

@Injectable({
  providedIn: 'root'
})
export class DynamicThemeService {


  initTheme = 0; // the first theme to be applied
  private theme = new BehaviorSubject<number>(this.initTheme);
  theme$ = this.theme.asObservable();

  constructor(private meta: Meta) { }

  setTheme(id: number): void {
    // set theme color
    this.theme.next(id);

    // set browser color
    const bodyStyles = window.getComputedStyle(document.body);
    const primaryColor = bodyStyles.getPropertyValue('--theme' + id.toString());
    this.meta.updateTag({ content: primaryColor }, 'name=theme-color');
  }
}
