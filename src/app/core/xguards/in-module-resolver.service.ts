/**
 * El flujo de los resolvers es asi: desk se carga de la url (browser) y se ejecuta su resolver en admin
 * despues cuando se hace click en un modulo se setea el activeModule y luego se cargan los menus de ese modulo,
 * pero se desencadena un redirect al init (in-module) de aquel module, este ya no vuelve a ejecutar los resolvers
 * porque ya se cargaron; desde ahi si se hace click en cualquier menu in-module se sete activeInModule pero esto no
 * vuelve a cargar los menus porque ya estan cargados.
 * Cuando se accede de la url del browser a un modulo (el cual redirije a un init in-module) o a un in-module, estos
 * cargan los menus si es necesario y setean los active automaticamente cuando se necesita.
 * RedirectResolver e InModuleResolver usan la mismas funcion setMenu que realiza este proceso. InModResolver ademas
 * controla que se ejecute el action respectivo de su ActiveInModule.
 */

import { Injectable } from '@angular/core';
import {
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  Router
} from '@angular/router';
import { from, of, Observable } from 'rxjs';
import { MenuService, MenuQuery } from '../xstate';
import { DeskMenuResolver } from '../admin/resolver/menu-resolver.service';
import { resolverPipe } from './common';



// ACTIONS

/**
 * load action corresponding to the current active in-modulee (action is loaded only once by in-module).
 */
@Injectable({
  providedIn: 'root'
})
export class ActionResolver implements Resolve<any> {

  constructor(
    private menuService: MenuService,
    private menuQuery: MenuQuery,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {

    if (this.menuQuery.isLoadedAction()) { // avoid reload actions
      return of('already loaded');
    }

    return resolverPipe(
      this.menuService.getAction(), // replayed Observable
      state.url,
      this.router
    );
  }

}


// MENUS

/**
 * This resolver just load menus of the module.
 * This method not implement Resolver interface because we need an extra param;
 * the fact that we dont import it, doesn't matter 'cause is just a class method
 */
@Injectable({
  providedIn: 'root'
})
export class ModuleMenuResolver {

  constructor(
    private menuService: MenuService,
    private menuQuery: MenuQuery,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot, routeParam: string): Observable<any> | Observable<never> {

    if (this.menuQuery.isLoaded(routeParam)) { // avoid reload menus
      return of('already loaded');
    }

    return resolverPipe(
      this.menuService.getModule(),
      state.url,
      this.router
    );
  }

}


// IN-MODULE

/**
 * Este resolver solo puede ser usado en Router paths
 * que representen menus dentro de un module (in-module).
 * Este resolver carga los menus cuando se accede directo de la url del browser.
 * Y ejecuta la respectiva ACTION siempre.
 */
@Injectable({
  providedIn: 'root'
})
export class InModuleMenuResolver implements Resolve<{ desk: any, modules: any, action: any }> {

  constructor(
    private menuService: MenuService,
    private menuQuery: MenuQuery,
    private deskResolver: DeskMenuResolver,
    private moduleResolver: ModuleMenuResolver,
    private actionResolver: ActionResolver
  ) { }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<{ desk: any, modules: any, action: any }> {

    // go back until find parent module parameter
    let cRoute = route;
    let param = null;
    while (!param) {
      cRoute = cRoute.parent;
      param = cRoute.paramMap.get('module');
    }

    // resolve menus
    const { desk, modules } = await this.setMenu(route, state, param);

    // resolve actions
    const { action } = await from(this.actionResolver.resolve(route, state)).toPromise();

    return { desk, modules, action };
  }




  /**
   * If desk is not loaded: load it and set activeModule.
   * If current module is not loaded: load it and set active-InModule.
   *
   * TODO: if possible make initial menu resolve in just one request( not one for desk an other for module menus)
   * But for this TODO, first analize if it is necesary, because both webserver back and front ends will be in the
   * same server, so maybe it is not necesary to make this TODO. After all, two little request maybe not are too much problem.
   */
  async setMenu(route: ActivatedRouteSnapshot, state: RouterStateSnapshot, param: string) {

    // load desk menus if necesary
    const desk = await from(this.deskResolver.resolve(route, state)).toPromise();

    // set active if necesary
    if (desk !== 'already loaded') { // when user type url in browser  admin/:module
      this.menuService.setActiveModule(param, 'resolver');
    }

    // load module menus if necesary
    const modules = await from(this.moduleResolver.resolve(route, state, param)).toPromise();

    // set active in-module if necesary (when module menus has recently loaded)
    const url = state.url.slice(1).split('/').slice(2, 4).join('/'); // get in-module url(type/something ex: model/inventario)
    if (url) {
      if (modules !== 'already loaded') {
        // when user type url (/admin/:module/type/something) in browser,
        // or access from other place but module menus was recently loaded
        this.menuService.setActiveInModule(url, 'resolver');
      }
    } else {
      // when user type url (/admin/:module) in browser and redirect to initial route was not made yet,
      // or when acces through click a desk icon that navigate to (/admin/:module) and redirect is not made yet.
      // In this case, it is sure that activeModule was set.
      this.menuService.setActiveInModule(this.menuQuery.getActiveModule().initial_route, 'resolver');
    }

    return { desk, modules };

  }


}
