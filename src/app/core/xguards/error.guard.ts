import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree, Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { urlSplit } from './common';
import { SessionQuery } from '../xstate';


/**
 * this guard allow to redirect to the intial requested page
 * after it was redirected to error page for any reason.
 * TODO: IMPROVE: an error ocurr when the connection with server is lost
 * so we have to give the user an alert like redirect to erro page. Improve
 * this by making an alert system when the connection with server is lost,
 * and other to know when the internet connection is lost, and a spceial
 * workaround: when the application is going to load(start app) redirect to
 * error page or a special system that is ok, i dont know you think how to
 * do this all system.
 */
@Injectable({
  providedIn: 'root'
})
export class ErrorGuard implements CanActivate {

  constructor(private sessionQuery: SessionQuery, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // console.log('erro guard');

    if (this.sessionQuery.conectionVerified()) {
      return this.redirect(state);
    }
    // console.log('quedate en error');
    return true;
  }

  redirect(state: RouterStateSnapshot) {
    const url = urlSplit(state.url, this.router); // get complete url
    if (url.queryParams.redirectURL) {
      // console.log('redirije a redirectURL');
      return this.router.createUrlTree([url.queryParams.redirectURL], { // note: this is like push to the current router
        queryParams: { ...url.queryParams, redirectURL: null }, // allow to conserve the query params and clean the redirectUrl
        fragment: url.fragment // allow to conserve the original fragment of the url
      });
    } else {
      // console.log('redirige a admin');
      this.router.navigate(['/admin']); // when user type directly error in browser URL
    }
  }

}
