import { Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { catchError, mergeMap } from 'rxjs/operators';


// COMMON METHODS FOR ROUTES

/**
 * divide an url string to path, to queyparams y fragment
 * back = 0, returns the complete path (default)
 * back = -1, return the path without the last segment (rigth segment)
 * back = -2, return the path without the last right two segments
 */
export function urlSplit(stateUrl: string, router: Router, back = 0) {
  const urlTree = router.parseUrl(stateUrl);
  const pathArray = urlTree.root.children.primary.segments.map(it => it.path);
  let urlWithoutParams = '/';

  if (back >= 0) { // complete path
    urlWithoutParams += pathArray.join('/');
  } else { // back path
    urlWithoutParams += pathArray.slice(0, back).join('/');
  }
  return {
    path: urlWithoutParams,
    queryParams: urlTree.queryParams,
    fragment: urlTree.fragment
  };
}


/**
 * It is just a wrapper that handles pipe to catch Error and MergeMap to an adecuate response to resolver
 * @param Obs$ The input observable to process
 * @param stateUrl the input url, comes from state.url in resolvers
 * @param router the Router injected in resolver in order to navigate
 */
export function resolverPipe<T>(Obs$: Observable<T>, stateUrl: string, router: Router): Observable<T> {
  return Obs$.pipe(
    // emmit undefined, when api fails (not found the requested model /admin/:module/:model).
    // Undefinied in mergeMap will redirect to same route and return EMPTY
    catchError(_ => {
      if (_.status === 404) {
        router.navigate(['/admin']); // TODO: try to redirect to an error page, avoid infinite loop
        return EMPTY;
      } else {
        return of(undefined);
      }
    }),

    // mergeMap: it merge all suscription, first it get the value from the source observable
    // then, other observable modifies that value, but we dont need to suscribe to this second
    // observable, mergeMap do it, and the outer value of MergeMap is the value returned from
    // the suscribe of the second observable.
    mergeMap(data => {
      // validation of data received from api, cathError emmit undefinied
      // when error in order to redirect and return EMPTY, we could add any other validation
      if (data) {
        return of(data); // just pass the same value
      } else {
        // go to the same path (equal to do nothing), to avoid "uncaught error"
        // that appears when no navigate(router.navigate) and only return EMPTY.
        const url = urlSplit(stateUrl, router);
        router.navigate([url.path], { queryParams: url.queryParams, fragment: url.fragment });
        // TODO: make an alert with snackbar or something. This "else" run once for every
        // resolver that calls it, to make sure that only run once, you could do only one
        // resolver and combine request (in paralell).
        return EMPTY;
      }
    })
  );
}


/**
 * popup to confirm the 'discard changed', return boolean
 * TODO: make a better popup please
 */
export function userConfirm(message?: string): boolean {
  const confirmation = window.confirm(message || 'Continuar?');
  return confirmation;
};
