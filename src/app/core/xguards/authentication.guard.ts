import { Injectable } from '@angular/core';
import {
  CanActivateChild, ActivatedRouteSnapshot,
  RouterStateSnapshot, UrlTree, Router, CanActivate
} from '@angular/router';
import { Observable } from 'rxjs';
import { SessionQuery } from '../xstate';
import { urlSplit } from './common';


/**
 * allows to check if user is logged in if
 * not redirect to login page, and then to
 * the original page user want to access
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate, CanActivateChild {

  constructor(private sessionQuery: SessionQuery, private router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // Child guar run only for the 'path' configured with it
    return this.checkAuthentication(state);
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // Child guard run for every 'path' in router, including componentless paths
    // console.log('child guard');

    return this.checkAuthentication(state);
  }

  // canLoad(route: Route, segments: UrlSegment[]):
  //   Observable<boolean> | Promise<boolean> | boolean {
  //   return this.checkAuthentication(this.router.routerState.snapshot); // not working
  // }

  checkAuthentication(state: RouterStateSnapshot) {
    const url = urlSplit(state.url, this.router); // get complete url

    // first check initial connection
    if (!this.sessionQuery.conectionVerified()) {
      // console.log('redirige a error');
      return this.router.createUrlTree(['/error'], { // push to router
        queryParams: { ...url.queryParams, redirectURL: url.path }, // allow to conserve the query params and the url
        fragment: url.fragment // allo to conserve the original fragment of the url
      });
    }

    // then check authentication
    if (!this.sessionQuery.isLoggedIn()) {
      // console.log('redirige a login');
      // redirect to login (whitout lossing params nor fragment), when the visitor has no access
      // priotitization: createUrlTree works by giving the guard closest to the root of the app the highest priority.
      return this.router.createUrlTree(['/login'], { // push to router
        queryParams: { ...url.queryParams, redirectURL: url.path }, // allow to conserve the query params and the url
        fragment: url.fragment // allo to conserve the original fragment of the url
      });
    }
    // console.log('redirige a la pagina solicitada');
    return true;
  }

}
