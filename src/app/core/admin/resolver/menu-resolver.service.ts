import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { MenuService, MenuQuery } from '../../xstate';
import { resolverPipe } from '../../xguards/common';


/** This resolver just load menus of desk */

@Injectable({
  providedIn: 'root'
})
export class DeskMenuResolver implements Resolve<any> {

  constructor(
    private menuService: MenuService,
    private menuQuery: MenuQuery,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {

    if (this.menuQuery.isLoaded('desk')) { // avoid reload menus
      return of('already loaded');
    }

    return resolverPipe(
      this.menuService.getDesk(),
      state.url,
      this.router
    );
  }

}
