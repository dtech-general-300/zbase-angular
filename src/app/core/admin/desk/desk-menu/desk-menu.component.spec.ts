import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeskMenuComponent } from './desk-menu.component';

describe('DeskMenuComponent', () => {
  let component: DeskMenuComponent;
  let fixture: ComponentFixture<DeskMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeskMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeskMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
