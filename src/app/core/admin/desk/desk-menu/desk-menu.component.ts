import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/core/xstate';


@Component({
  selector: 'app-desk-menu',
  templateUrl: './desk-menu.component.html',
  styleUrls: ['./desk-menu.component.scss']
})
export class DeskMenuComponent implements OnInit {

  @Input() groups: any[];

  // responsive
  basicSpace = '7px'; // left padding of group name
  lateral = { global: '10%', mobile: '7px' }; // espacios laterales del bloque de cards
  top = { global: '5%', mobile: '7px' }; // espacio superior del bloque de cards
  space = { global: '0px 7px 14px 7px', mobile: '0px 7px 7px 7px' }; // espacio entre cards


  constructor(
    private menuService: MenuService,
  ) { }

  ngOnInit() {
  }

  go(clickedModule) {
    this.menuService.setActiveModule(clickedModule);
  }

}
