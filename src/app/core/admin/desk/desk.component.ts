import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { DynamicThemeService } from '../../xservices';
import { Observable, Subject } from 'rxjs';
import { MenuQuery, SessionService } from '../../xstate';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';



@Component({
  selector: 'app-desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeskComponent implements OnInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  groups; // the menu data with groups-modules-models-etc
  theme$: Observable<number>; // the observable for themes

  constructor(
    private dynamic: DynamicThemeService,
    private menuQuery: MenuQuery,
    private sessionSerivce: SessionService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.setTheme(this.dynamic.initTheme); // set the default theme
    this.theme$ = this.dynamic.theme$; // get the observable from service
    this.groups = this.menuQuery.getLoadedMenu('desk');
  }

  setTheme(id: number) {
    this.dynamic.setTheme(id);
  }

  logout() {
    if (this.menuQuery.canFlag()) {
      this.sessionSerivce.logout().pipe(takeUntil(this.u$)).subscribe(data => {
        this.router.navigate(['/admin']);
      });
    }
  }



  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }


}
