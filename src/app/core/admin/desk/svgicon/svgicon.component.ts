import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-svgicon',
  templateUrl: './svgicon.component.html',
  styleUrls: ['./svgicon.component.scss']
})
export class SvgiconComponent implements OnInit {

  colors = {
    forro: '#85bc94',
    forroBajo: '#6b9777',

  };

  constructor() { }

  ngOnInit() {
  }

}
