import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from '../xguards';
import { DeskComponent } from './desk/desk.component';
import { DeskMenuResolver } from './resolver/menu-resolver.service';


const routes: Routes = [
  {
    path: '',
    canActivateChild: [AuthenticationGuard],
    children: [
      {
        path: '',
        component: DeskComponent,
        resolve: {
          menu: DeskMenuResolver
        },
      },
      {
        path: ':module',
        loadChildren: () => import('../module').then(m => m.ModuleModule),
        data: { preload: true, delay: 300 }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
