import { NgModule } from '@angular/core';
import { AdminRoutingModule } from './admin-routing.module';
import { DeskComponent } from './desk/desk.component';
import { MaterialModule } from '../xshared';
import { DeskMenuComponent } from './desk/desk-menu/desk-menu.component';


@NgModule({
  declarations: [
    DeskComponent,
    DeskMenuComponent,
  ],
  imports: [
    MaterialModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
