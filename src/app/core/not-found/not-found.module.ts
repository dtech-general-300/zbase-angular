// this is a simple lazy module;
// it shows a component

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PageErrorComponent } from './page-error/page-error.component';
import { ErrorGuard } from '../xguards';


@NgModule({
  declarations: [PageNotFoundComponent, PageErrorComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: PageErrorComponent, canActivate: [ErrorGuard] },
      { path: 'not-found', component: PageNotFoundComponent }
    ]),
  ],
  exports: [RouterModule]
})
export class NotFoundModule { }
