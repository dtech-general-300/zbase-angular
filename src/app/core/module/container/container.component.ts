import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuQuery, MenuService } from '../../xstate';



/**
 * Este component solo es un contenedor del app-module y permite realizar operaciones
 * de resolvers de menu e inicializar los mismos en menuItems para enviarlos al app-module
 */
@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerComponent implements OnInit {

  menuItems = [];

  constructor(
    private menuQuery: MenuQuery,
    private route: ActivatedRoute,
    private menuService: MenuService
  ) { }

  ngOnInit() {
    // this always get the params when route changed (only works in a this level "/admin/:module")
    // this.route.paramMap.pipe(takeUntil(this.u$)).subscribe((params: ParamMap) => {
    //    console.log(params.get('module'));
    // });

    // getting data sent to this route component in Route paths
    // this.someVar = this.route.snapshot.data.something;

    // only runs once
    // this.route.snapshot.paramMap.get('module');

    // initialization
    this.menuItems = this.menuQuery.getLoadedMenu(
      this.route.snapshot.paramMap.get('module')
    );
  }

}
