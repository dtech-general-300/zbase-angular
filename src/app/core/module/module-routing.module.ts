import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContainerComponent } from './container/container.component';
import { RedirectResolverGuard } from './resolver/resolver.guard';


const routes: Routes = [
  {
    path: '',
    component: ContainerComponent,
    children: [
      {
        path: '',
        canActivate: [RedirectResolverGuard],
      },
      {
        path: 'model',
        loadChildren: () => import('../model').then(m => m.ModelModule),
        data: { preload: true, delay: 30 } // this preload start after admin was loaded
      },
      // {
      //   path: 'dashboard',
      //   loadChildren: () => import('../dashboard').then(m => m.DashboardMod),
      //   data: { preload: true, delay: true }
      // },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModuleRoutingModule { }
