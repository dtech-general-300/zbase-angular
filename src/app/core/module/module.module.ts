import { NgModule } from '@angular/core';

import { ModuleRoutingModule } from './module-routing.module';
import { ModuleComponent } from './module/module.component';
import { ToolbarMenuComponent } from './module/toolbar-menu/toolbar-menu.component';
import { SubMenusComponent } from './module/toolbar-menu/sub-menus/sub-menus.component';
import { VerticalMenuComponent } from './module/vertical-menu/vertical-menu.component';
import { MaterialModule } from '../xshared';
import { ContainerComponent } from './container/container.component';


@NgModule({
  declarations: [
    ContainerComponent,
    ModuleComponent,
    ToolbarMenuComponent,
    SubMenusComponent,
    VerticalMenuComponent,
  ],
  imports: [
    MaterialModule,
    ModuleRoutingModule
  ]
})
export class ModuleModule { }
