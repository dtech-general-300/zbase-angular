import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MenuItem, MenuService } from 'src/app/core/xstate';


@Component({
  selector: 'app-sub-menus',
  templateUrl: './sub-menus.component.html',
  styleUrls: ['./sub-menus.component.scss']
})
export class SubMenusComponent implements OnInit {

  // recursive menu
  @Input() items: MenuItem[];
  @Input() depth: number;
  @Input() icons: boolean; // show or hide icons
  @ViewChild('childMenu', { static: true }) public childMenu: any;

  constructor(private menuService: MenuService) { }

  ngOnInit() {
  }

  go(clickedInModule) {
    this.menuService.setActiveInModule(clickedInModule);
  }
}
