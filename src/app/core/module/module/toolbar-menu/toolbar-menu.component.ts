import {
  Component, Input, ViewChild, ElementRef, HostListener, QueryList,
  ViewChildren, ChangeDetectorRef, OnChanges, AfterContentInit, SimpleChanges
} from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MenuItem, MenuService } from 'src/app/core/xstate';


@Component({
  selector: 'app-toolbar-menu',
  templateUrl: './toolbar-menu.component.html',
  styleUrls: ['./toolbar-menu.component.scss']
})
export class ToolbarMenuComponent implements AfterContentInit, OnChanges {

  @Input() items: MenuItem[]; // received items
  @Input() slow: number[]; // detect changes in images or other tags, it runs ngOnChanges
  @Input() icons: boolean; // show or hide icons
  @ViewChild('container', { static: true }) container: ElementRef; // div container of buttons
  @ViewChildren('button') buttons: QueryList<MatButton>; // list of button inside container

  zItems: MenuItem[]; // array of items used in template
  tranparent = true; // transparent when initilize, no transp when setItems, this improve UX
  roundedWindow: number; // rounded window size
  pxChange = 21; // # of px space to get a response

  flagShowHide = false; // detect when component pass from hide to show in changes like fxHide
  widths: number[] = []; // array with widths of rendered buttons
  moreWidth = 0; // width of more button

  more: MenuItem = { // more button prototype
    id: -1,
    ref: 'more_button_menu',
    name: '',
    sequence: 1000000,
    icon: 'more_horiz',
    menu_type: 'more',
    children: []
  };


  constructor(private changeDetector: ChangeDetectorRef, private menuService: MenuService) { }


  // when slow @Input change this method run, slow[1] has the total changes
  // of slow in the parent, and slow[0] is a counter. This ngOnChanges run
  // exactly slow[1] + 1 times, this 1 extra time is because the instantation
  // of this component, so if in parent slow is [0,4] it means parent has 3
  // img tag, or button that appear or dissapear that applies childOnChange()
  // plus the call of childOnChange in the ngAfterContentInit (setTimeout).
  // so ngOnChange run 1+3+1=5 times, that is because in instantation an extra
  // change of the counter has effect, from undefined to 0. So in the parent
  // you must put [ always 0 , number of elements with childOnChange ]
  ngOnChanges(changes: SimpleChanges) {
    if (changes.hasOwnProperty('slow') && this.slow[0] === this.slow[1]) {
      this.setItems();
    }
  }

  ngAfterContentInit() {
    this.Initialize(); // get widths at start
  }


  @HostListener('window:resize', ['$event'])
  onResize(event?: any) {
    // show-hide container, to react correctly when this component pass from hide to show (fx.Hide)
    if (this.flagShowHide && this.container.nativeElement.offsetWidth > 0) { // showed
      this.flagShowHide = false;
      this.Initialize(); // getwidths
      this.setItems(); // render
    }
    if (!this.flagShowHide && this.container.nativeElement.offsetWidth === 0) { // hidden
      this.flagShowHide = true;
    }

    // this code run every pxChange when window size changes
    const w = Math.floor(window.innerWidth / this.pxChange) * this.pxChange;
    if (this.roundedWindow !== w) {
      this.roundedWindow = w;
      this.setItems(); // we already have widths, now just render
    }
  }

  // @HostListener('window:orientationchange', ['$event'])
  // onOrientationChange(event?: any) {
  //   // this.Initialize(); // not proven
  //   // this.setItems(); // not proven
  // }


  // just get real button widths
  Initialize() {
    this.tranparent = true; // hide text

    // render all buttons
    this.zItems = [...this.items, this.more];
    this.changeDetector.detectChanges();

    // get rendered buttons Widths
    const x = [];
    this.buttons.forEach(button => {
      x.push(button._elementRef.nativeElement.offsetWidth);
    });
    this.widths = x.slice(0, x.length - 1);
    this.moreWidth = x[x.length - 1];

  }

  // render only the items that accommodate inside maxAvailable width
  // considering the more button width
  setItems(): any {
    let index = 0; // limit index
    let visible = false; // more button visible or not
    const maxWidth = this.container.nativeElement.parentElement.offsetWidth; // maxAvailableWidth
    let acum = this.pxChange; // acumulator of widths
    // acum = pxChange allows an offset before maxAvailable Width (it reduces maxAvaiWidth)

    // get limit index inside maxAvailableWidth and check if moreButton is visible
    for (const width of this.widths) {
      acum += width;
      if ((index === this.widths.length - 1) && (acum < maxWidth)) {
        visible = false;
        break;
      }
      if (acum >= maxWidth) {
        visible = true;
        index--;
        if (acum - width + this.moreWidth >= maxWidth) { index--; }
        break;
      }
      index++;
    }

    // only moreButton in zItems when no space is Available
    if (index < 0) {
      const more = this.more;
      more.children = this.items;
      this.zItems = [more];
      this.tranparent = false; // show text
      return;
    }

    // if space available, set an array of items and put it in zItems
    let xtems = this.items.slice(0, index + 1);
    if (visible) {
      const more = this.more;
      more.children = this.items.slice(index + 1);
      xtems = [...xtems, more];
    }

    this.zItems = xtems; // angular render this new zItem automatically
    this.tranparent = false; // show text
  }

  go(clickedInModule) {
    this.menuService.setActiveInModule(clickedInModule);
  }

}
