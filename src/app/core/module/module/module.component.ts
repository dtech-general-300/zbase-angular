import { Component, OnInit, Input, TemplateRef, AfterContentInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MediaObserver, MediaChange } from '@angular/flex-layout';
import { DynamicThemeService } from '../../xservices';
import { Observable, Subject } from 'rxjs';
import { MenuItem, SessionService, MenuQuery } from '../../xstate';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss']
})
export class ModuleComponent implements OnInit, AfterContentInit, OnDestroy {

  u$ = new Subject<void>(); // unsuscriber

  @Input() items: MenuItem[];
  @Input() ref: TemplateRef<any>;

  // slowFlag
  // [counter=0, number of calls] when this flag change, ngOnChanges
  // in toolbar-menu runs. Use with img tags or others.
  // counter: number of changes done, allways start at zero
  // number of calls: total of calls of the childOnChange method
  slowFlag = [0, 1];

  opened = false; // sidenav opened
  activeMedia = ''; // current media that is active
  theme$: Observable<number>; // observable for theme


  constructor(
    private mediaObserver: MediaObserver,
    private menuQuery: MenuQuery,
    private dynamic: DynamicThemeService,
    private cdr: ChangeDetectorRef,
    private sessionSerivce: SessionService,
    private router: Router,
  ) {
    // tslint:disable-next-line: deprecation
    mediaObserver.media$.pipe(takeUntil(this.u$)).subscribe((change: MediaChange) => {
      this.activeMedia = change ? `${change.mqAlias}` : '';
      if (change.mqAlias !== 'xs') {
        this.opened = false;
      }
    });
  }

  ngOnInit() {
    this.dynamic.setTheme(this.dynamic.initTheme); // set the default theme
    this.theme$ = this.dynamic.theme$; // get the observable from service
  }


  // For toolbar-menu
  ngAfterContentInit() {
    // setTimeOut allow that method childOnChange has effect in change the slowFlag variable to run ngOnChange
    // in child. This allows to send at least one change (if the html has no lag img tag or dynamic buttons).
    // markForCheck allow OnPush detection works with this setTimeout
    setTimeout(() => { this.childOnChange(); this.cdr.markForCheck(); }, 0);
  }

  // For toolbar-menu
  childOnChange() {
    // put this method everywhere the toolbar changes
    // adding elements, laoding imgs, removing something
    this.slowFlag = [this.slowFlag[0] + 1, this.slowFlag[1]];
  }

  logout() {
    if (this.menuQuery.canFlag()) {
      this.sessionSerivce.logout().pipe(takeUntil(this.u$)).subscribe(data => {
        this.router.navigate(['/admin']);
      });
    }
  }

  // For change theme
  setTheme(id: number) {
    this.dynamic.setTheme(id);
  }


  // DESTROY
  ngOnDestroy() {
    this.u$.next(); // unsuscribe
    this.u$.complete(); // unsuscribe
  }

}
