import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSidenav } from '@angular/material';
import { Animations } from '../../../xshared';
import { MenuItem, MenuService } from 'src/app/core/xstate';


@Component({
  selector: 'app-vertical-menu',
  templateUrl: './vertical-menu.component.html',
  styleUrls: ['./vertical-menu.component.scss'],
  animations: [Animations.rotate90, Animations.fadedGrowY]
})
export class VerticalMenuComponent implements OnInit {

  expanded: boolean;
  @Input() item: MenuItem;
  @Input() depth: number;
  @Input() icons: boolean; // show or hide icons
  @Input() sidenav: MatSidenav; // the original sidenav

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private menuService: MenuService
  ) {
    if (this.depth === undefined) { this.depth = 0; }
  }

  ngOnInit() {
  }

  // this method apply a class to differentiate the active item, considering the router
  isActive(): boolean {
    let active = false;
    if (this.item.menu_type === 'model') {
      active = this.router.isActive(
        this.router.createUrlTree([this.item.menu_type, this.item.client_route], { relativeTo: this.route }).toString(),
        true
      );
    }
    return active;
  }

  // redirect to route, or just expand menu depending on children of item
  onItemSelected(item: MenuItem) {
    if (!item.children || !item.children.length) { // clickable item
      this.menuService.setActiveInModule(item);
      this.router.navigate([item.menu_type, item.client_route], { relativeTo: this.route });
      this.sidenav.close();
    }
    if (item.children && item.children.length) { // expandable item
      this.expanded = !this.expanded;
    }
  }

}
