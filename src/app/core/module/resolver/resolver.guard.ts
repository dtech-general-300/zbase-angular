import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot, RouterStateSnapshot,
  UrlTree, Router
} from '@angular/router';
import { MenuQuery } from '../../xstate';
import { InModuleMenuResolver } from '../../xguards';


/**
 * A CanActivate works as Series resolver that allows to wait for sequential run of other resolvers
 * (no matter if resolvers return promises or observables, 'cause we use from().toPromise)
 *
 * Also redirect to initial_route of the module menu.
 * from  /admin/:module  redirectTo  /admin/:module/type/:model
 *
 * initial route = menu_type / clien_route
 *
 */
@Injectable({
  providedIn: 'root'
})
export class RedirectResolverGuard implements CanActivate {

  constructor(
    private router: Router,
    private menuQuery: MenuQuery,
    private inModuleResolver: InModuleMenuResolver
  ) { }

  async canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean | UrlTree> {

    await this.inModuleResolver.setMenu(next, state, next.paramMap.get('module'));

    // go to initial route (initial route = menu_type / clien_route), so an action will run in its resolver
    return this.router.createUrlTree(
      [state.url + '/' + this.menuQuery.getActiveModule().initial_route],
    );
  }

}


