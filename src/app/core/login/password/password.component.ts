import { Component, OnInit, ViewChild, TemplateRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { hasErrors, canSubmit, BaseForm } from '../../xshared';
import { SessionService } from '../../xstate';


@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordComponent extends BaseForm implements OnInit {

  status = 0; // 0=untouched 1=loading 2=error 3=success // help to ui state
  hidePass = true; // apply to icon a hide-show password feature
  message = ''; // snackbar message to show with binding
  @ViewChild('snackTemplate', { static: false }) snackTemplate: TemplateRef<any>;

  constructor(
    private fb: FormBuilder,
    private session: SessionService,
    private snack: MatSnackBar,
    private cdr: ChangeDetectorRef
  ) { super(); }

  ngOnInit() {
    this.group = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  @hasErrors([['required', 'Email requerido']])
  eEmail(x) { return this.fc.email; }

  @hasErrors([['required', 'Contraseña requerida']])
  ePass(x) { return this.fc.password; }

  @canSubmit
  loginSubmit(x) {
    this.status = 1;
    this.session.login(this.fv.email.trim(), this.fv.password.trim())
      .subscribe(
        () => {
          this.status = 3;
        },
        error => {
          this.status = 2;
          switch (error.error.message) {
            case 'invalid':
              this.snackBar('Credenciales inválidas'); break;
            case 'internal':
              this.snackBar('Error del Servidor'); break;
            default: // undefined
              this.snackBar('Falló conexión con el servidor'); break;
          }
        }
      ).add(_ => this.cdr.markForCheck());
  }

  snackBar(message: string) {
    this.message = message;
    this.snack.openFromTemplate(this.snackTemplate, {
      duration: 1700
    });
  }

}
