// this is a lazy modulue that shows the passwordComponent
// in order to the user get access to the system. It only has
// one component and work with akita session service

import { NgModule } from '@angular/core';
import { PasswordComponent } from './password/password.component';
import { ReactFormModule } from '../xshared';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    PasswordComponent
  ],
  imports: [
    ReactFormModule,
    RouterModule.forChild([
      { path: '', component: PasswordComponent }
    ]),
  ],
  exports: [RouterModule]
})
export class LoginModule { }
