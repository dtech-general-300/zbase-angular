import { NgModule, Injectable } from '@angular/core';
import { Routes, RouterModule, PreloadingStrategy, Route } from '@angular/router';
import { Observable, timer, of, ObservableInput } from 'rxjs';
import { flatMap } from 'rxjs/operators';


// CUSTOM PRELOAD

@Injectable({ providedIn: 'root' })
export class CustomPreloadingStrategy implements PreloadingStrategy {
  preload(route: Route, load: () => Observable<any>): Observable<any> {
    const loadRoute = (delay) => delay
      ? timer(delay).pipe(flatMap(_ => load()))
      : load();
    return route.data && route.data.preload
      ? loadRoute(route.data.delay)
      : of(null);
  }
}



// ROUTING MODULE

const routes: Routes = [
  { path: '', redirectTo: '/admin', pathMatch: 'full' },
  { path: 'admin', loadChildren: () => import('./core/admin').then(m => m.AdminModule) },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: 'login', loadChildren: () => import('./core/login').then(m => m.LoginModule) },
  { path: 'error', loadChildren: () => import('./core/not-found').then(m => m.NotFoundModule) },
  { path: '**', redirectTo: '/error/not-found' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy: CustomPreloadingStrategy,
      onSameUrlNavigation: 'reload', // you must use runGuardsAndResolvers: 'always' on the path you want to reload
    })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// in wild card ** redirects to not found, otherwise it will load at start app
