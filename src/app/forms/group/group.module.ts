import { NgModule } from '@angular/core';
import { GroupComponent } from './group/group.component';
import { ReactFormModule } from 'src/app/core/xshared';



@NgModule({
  declarations: [GroupComponent],
  imports: [
    ReactFormModule
  ],
  entryComponents: [GroupComponent]
})
export class GroupModule {
  // Define entry property to access entry component in loader
  // service. This is like the root component of this module
  static entry = GroupComponent;
}
