import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { hasErrors, GenericForm } from 'src/app/core/xshared';
import { Validators } from '@angular/forms';


/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent extends GenericForm implements OnInit, OnDestroy {

  constructor(private injector: Injector) { super(injector); }

  ngOnInit() {
    this.group = this.fb.group({
      id: [null, Validators.required],
      name: [null, Validators.required],
    });
    super.ngOnInit();
  }

  @hasErrors([['required', 'Nombre requerido']])
  eName(x) { return this.fc.name; }

  submitForm() {
  }

  ngOnDestroy() {
    console.log('destroy group');
  }
}
