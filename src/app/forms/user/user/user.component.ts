import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IForm } from 'src/app/core/xinterface';
import { Observable } from 'rxjs';
// import { GenericQuery } from 'src/app/core/model';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy, IForm {

  @Input() data: any;
  fields$: Observable<any>;
  record$: Observable<any>;

  constructor(
    // private gQuery: GenericQuery,
  ) { }

  ngOnInit() {
    // this.record$ = this.gQuery.selectActive();

  }

  ngOnDestroy() {
  }

}
