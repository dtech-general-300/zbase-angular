import { NgModule } from '@angular/core';
import { UserComponent } from './user/user.component';
import { ReactFormModule } from 'src/app/core/xshared';



@NgModule({
  declarations: [UserComponent],
  imports: [
    ReactFormModule
  ],
  entryComponents: [UserComponent]
})
export class UserModule {
  // Define entry property to access entry component in loader service
  static entry = UserComponent;
}
