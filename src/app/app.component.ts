import { Component, HostListener } from '@angular/core';
import { Event, Router, NavigationEnd, NavigationCancel, NavigationError } from '@angular/router';
import { fadeOut } from './core/xshared';
import { takeWhile } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GeneralQuery } from './core/xstate';
import { ClickOutsideService } from './core/xservices';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeOut]
})
export class AppComponent {
  title = 'zbase';
  firstLoad = true;
  cargando$: Observable<number>;

  constructor(
    private router: Router,
    private generalQuery: GeneralQuery,
    private clickOutside: ClickOutsideService
  ) {
    // allow app initial spinner or effect (it auto unsuscribe)
    this.router.events.pipe(takeWhile(_ => this.firstLoad === true)).subscribe((event: Event) => {
      switch (true) {
        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: { this.firstLoad = false; break; }
        default: { break; }
      }
    });

    // cargando box
    this.cargando$ = this.generalQuery.cargando$;
  }


  // getAnimationData(outlet: RouterOutlet) {
  //   return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  // }


  // Click Outside
  @HostListener('document:click', ['$event.target'])
  documentClick(target: any): void {
    this.clickOutside.documentClickedTarget.next(target);
  }
}
