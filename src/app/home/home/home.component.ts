import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit {

  config = {
    position: 'top'
  };
  constructor() { }

  ngOnInit() {
  }


  onClick() {
    this.config = {
      position: 'bottom'
    }
  }

}
