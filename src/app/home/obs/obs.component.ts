import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';


// solo cambia porque async tiene dentro de su definicion un markForCheck()
@Component({
  selector: 'app-list',
  template: `
     <div *ngFor="let item of items | async">{{item.title}}</div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit {

  @Input() items: Observable<any>;
  
  _items: any[];
  
  ngOnInit() {
    this.items.subscribe(items => {
      this._items = items;
    });
  }

}

@Component({
  selector: 'app-obs',
  templateUrl: './obs.component.html',
  styleUrls: ['./obs.component.scss']
})
export class ObsComponent implements OnInit {

  items = [];
  items$ = new BehaviorSubject(this.items);
  
  constructor() { }

  ngOnInit() {
  }

  add() {
    this.items.push({ title: Math.random() })
    this.items$.next(this.items);
  }

}


