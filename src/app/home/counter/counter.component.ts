import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent implements OnInit {
  count = 0;

  constructor() { }

  ngOnInit() {
    setTimeout(() => this.count = 5, 10000);
  }

  add() {
    console.log('add: ' + this.count);
  }

}
