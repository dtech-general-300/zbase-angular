import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChildComponent implements OnInit {

  @Input() name: string;

  @Input() config;

  constructor() { }

  ngOnInit() {
  }

  get runChangeDetection() {
    console.log('Checking the view');

    console.time('holissssss')
    for (let index = 0; index < 1000 * 100; index++) {
      let element = 5;
      element += element * 8;
      element += element * 8;
      if (element !== null) {
        let t = [0, 1, 2, 3, 3, 4, 45, 3, 45, 45, 3, 45, 34, 5, 34, 5, 345, 34, 5];
        t.forEach((s, i) => {
          s = Math.sin(s);
        })
      }
    }
    console.timeEnd('holissssss')


    return true;
  }

}
