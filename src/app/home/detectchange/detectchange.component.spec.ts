import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetectchangeComponent } from './detectchange.component';

describe('DetectchangeComponent', () => {
  let component: DetectchangeComponent;
  let fixture: ComponentFixture<DetectchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetectchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetectchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
