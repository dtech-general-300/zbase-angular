import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-detectchange',
  templateUrl: './detectchange.component.html',
  styleUrls: ['./detectchange.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetectchangeComponent implements OnInit {

  count = 0;

  constructor(private cdr: ChangeDetectorRef) {

    setTimeout(() => {
      this.count = 5;
      console.log('holi');
      // this.cdr.detectChanges();
      // this.cdr.markForCheck();
    }, 1000);

  }

  ngOnInit() {
  }

}
