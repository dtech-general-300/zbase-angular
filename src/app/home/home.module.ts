import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home/home.component';
import { ChildComponent } from './child/child.component';
import { CounterComponent } from './counter/counter.component';
import { DetectchangeComponent } from './detectchange/detectchange.component';
import { ObsComponent, ListComponent } from './obs/obs.component';


@NgModule({
  declarations: [HomeComponent, ListComponent, ChildComponent, CounterComponent, DetectchangeComponent, ObsComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
