import { Component, Injector } from '@angular/core';
import { GenericList } from 'src/app/core/xshared';
import { Validators } from '@angular/forms';


/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-ilist',
  templateUrl: './ilist.component.html',
  styleUrls: ['./ilist.component.scss']
})
export class IlistComponent extends GenericList {

  constructor(private injector: Injector) {
    super(injector);

    this.definition = {
      name: ['']
    };
  }

  // @fieldErrors([['required', 'Nombre requerido']])
  // eName() { return this.fc.name; }

}
