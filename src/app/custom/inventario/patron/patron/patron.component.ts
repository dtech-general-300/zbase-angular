import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { GenericForm, fieldErrors } from 'src/app/core/xshared';


/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-patron',
  templateUrl: './patron.component.html',
  styleUrls: ['./patron.component.scss']
})
export class PatronComponent extends GenericForm implements OnInit {

  constructor(private injector: Injector) { super(injector); }

  ngOnInit() {
    this.definition = {
      name: [null, Validators.required],
      patronlinea_ids: {
        str_name: [null],
        atributo_id: [null],
        // valor_ids: {
        //   name: ['']
        // }
      }
    };
    super.ngOnInit();
  }

  @fieldErrors([['required', 'Nombre requerido']])
  eName() { return this.fc.name; }

  submitForm() {
  }

}
