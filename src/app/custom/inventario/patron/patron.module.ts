import { NgModule } from '@angular/core';
import { ReactFormModule } from 'src/app/core/xshared';
import { WidgetsModule } from 'src/app/core/model';
import { PatronComponent } from './patron/patron.component';


@NgModule({
  declarations: [PatronComponent],
  imports: [
    ReactFormModule,
    WidgetsModule
  ],
  entryComponents: [PatronComponent]
})
export class PatronModule {
  static form = PatronComponent;
}
