import { NgModule } from '@angular/core';
import { ReactFormModule } from 'src/app/core/xshared';
import { WidgetsModule } from 'src/app/core/model';
import { IlistComponent } from './ilist/ilist.component';
import { IformComponent } from './iform/iform.component';


@NgModule({
  declarations: [IformComponent, IlistComponent],
  imports: [
    ReactFormModule,
    WidgetsModule
  ],
  entryComponents: [IformComponent, IlistComponent]
})
export class PatronlineaModule {
  static form = IformComponent;
  static list = IlistComponent;
}
