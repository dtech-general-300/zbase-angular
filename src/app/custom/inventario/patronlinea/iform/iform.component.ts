import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { GenericForm, fieldErrors } from 'src/app/core/xshared';


/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-iform',
  templateUrl: './iform.component.html',
  styleUrls: ['./iform.component.scss']
})
export class IformComponent extends GenericForm implements OnInit {

  constructor(private injector: Injector) { super(injector); }

  ngOnInit() {
    this.definition = {
      patron_id: [null],
      atributo_id: [null],
      valor_ids: {
        name: ['']
      }
    };
    super.ngOnInit();
  }

  // @fieldErrors([['required', 'Nombre requerido']])
  // eName() { return this.fc.name; }

  submitForm() {
  }

  async ok() {
    // await this.ready();
    console.log(this.group);
    console.log(this.group.pristine);
    console.table(this.group.getRawValue().attribute_line_ids);
  }

}
