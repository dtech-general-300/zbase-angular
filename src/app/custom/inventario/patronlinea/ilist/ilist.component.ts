import { Component, OnInit, Injector } from '@angular/core';
import { GenericList } from 'src/app/core/xshared';


/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-ilist',
  templateUrl: './ilist.component.html',
  styleUrls: ['./ilist.component.scss']
})
export class IlistComponent extends GenericList {

  constructor(private injector: Injector) {
    super(injector);

    this.definition = {
      str_name: [''],
      patron_id: [null],
      atributo_id: [null],
      // valor_ids: {
      //   name: ['']
      // }
    };
  }

  // @fieldErrors([['required', 'Nombre requerido']])
  // eName() { return this.fc.name; }

}
