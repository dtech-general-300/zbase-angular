import { NgModule } from '@angular/core';
import { ProductComponent } from './product/product.component';
import { ReactFormModule } from 'src/app/core/xshared';
import { WidgetsModule } from 'src/app/core/model';



@NgModule({
  declarations: [ProductComponent],
  imports: [
    ReactFormModule,
    WidgetsModule
  ],
  entryComponents: [ProductComponent]
})
export class ProductModule {
  // Define entry property to access entry component in loader
  // service. This is like the root component of this module
  static form = ProductComponent;
}
