import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { GenericForm, fieldErrors } from 'src/app/core/xshared';

/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss'],
})
export class ProductComponent extends GenericForm implements OnInit {

  constructor(private injector: Injector) { super(injector); }

  ngOnInit() {
    this.definition = {
      name: [null, Validators.required],
      attribute_line_ids: {
        name: [''],
      }
    };
    super.ngOnInit();
  }


  @fieldErrors([['required', 'Nombre requerido']])
  eName() { return this.fc.name; }

  submitForm() {
  }

  async ok() {
    await this.ready();
    console.log(this.group);
    console.log(this.group.pristine);
    console.table(this.group.getRawValue().attribute_line_ids);
  }
}
