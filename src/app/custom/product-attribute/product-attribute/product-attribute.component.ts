import { Component, OnInit, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { GenericForm, fieldErrors } from 'src/app/core/xshared';

/** This component is affected by OnPush change detection */
@Component({
  selector: 'app-product-attribute',
  templateUrl: './product-attribute.component.html',
  styleUrls: ['./product-attribute.component.scss']
})
export class ProductAttributeComponent extends GenericForm implements OnInit {

  constructor(private injector: Injector) { super(injector); }

  ngOnInit() {
    this.definition = {
      name: [null, Validators.required],
      product_id: [null],
    };
    super.ngOnInit();
  }

  @fieldErrors([['required', 'Nombre requerido']])
  eName() { return this.fc.name; }

  submitForm() {
  }

}
