import { NgModule } from '@angular/core';
import { ProductAttributeComponent } from './product-attribute/product-attribute.component';
import { ReactFormModule } from 'src/app/core/xshared';
import { WidgetsModule } from 'src/app/core/model';



@NgModule({
  declarations: [ProductAttributeComponent],
  imports: [
    ReactFormModule,
    WidgetsModule
  ],
  entryComponents: [ProductAttributeComponent]
})
export class ProductAttributeModule {
  // Define entry property to access entry component in loader
  // service. This is like the root component of this module
  static form = ProductAttributeComponent;
}
